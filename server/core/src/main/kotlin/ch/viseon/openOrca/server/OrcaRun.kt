/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.server

import ch.viseon.openOrca.server.impl.CommandListExecutor
import ch.viseon.openOrca.server.impl.OrcaSourceImpl
import ch.viseon.openOrca.server.impl.loggerFor
import ch.viseon.openOrca.share.CommandData
import ch.viseon.openOrca.share.Event
import ch.viseon.openOrca.share.Source
import ch.viseon.openOrca.share.command.CommandApplication
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import java.util.logging.Level

class OrcaRun(

    private val commandListExecutor: CommandListExecutor,
    private val requestProcessor: (OrcaSource) -> Observable<CommandData>,
    private val storeLifecycle: StoreLifecycle) {

  companion object {
    @JvmStatic
    val LOGGER = loggerFor<OrcaRun>()
  }


  fun execute(request: Request, responseProcessor: Response) {
    val proxySubject = ReplaySubject.create<Event>(100)

    val modelStore = storeLifecycle.getStore()
    val orcaSource = OrcaSourceImpl(modelStore, proxySubject)

    val responseCommandSubject = PublishSubject.create<CommandApplication>()

    val responseCommands = requestProcessor(orcaSource)

    val requestcommands = request.readCommands().doOnNext { LOGGER.fine { "Process request command: $it" } }

    val events =
        commandListExecutor.execute(modelStore, Observable.concat(requestcommands, responseCommands))
            .doOnNext {
              responseCommandSubject.onNext(it)
            }

    responseProcessor.sendCommands(
        responseCommandSubject
            .filter { it.sendCommand() }
            .map { it.commandData }
            .filter { it.source == Source.RESPONSE }
    )

    events
        .flatMap { Observable.fromIterable(it.events) }
        .doOnError { LOGGER.log(Level.SEVERE, "Error during command processing", it) }
        .doOnNext { LOGGER.finer { "Processing event $it" } }
        .doOnComplete {
          storeLifecycle.updateStore(modelStore)
          responseCommandSubject.onComplete()
        }
        //This method call will "block" until all collected commands are "send" to the subscribers.
        //All commands produced during execution will be added to the list of items to emit in the subject
        .subscribe(proxySubject)

    proxySubject.onComplete()

  }

}

