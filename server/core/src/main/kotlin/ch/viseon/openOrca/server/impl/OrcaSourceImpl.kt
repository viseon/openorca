/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.server.impl

import ch.viseon.openOrca.server.OrcaSource
import ch.viseon.openOrca.share.*
import io.reactivex.Observable

/**
 * This class acts as a facade.
 */
/*
 * The eventSubject serves as multiplexer to various subscriptions.
 */
class OrcaSourceImpl(private val modelStore: ModelStore,
                     private val eventSubject: Observable<Event>) : OrcaSource {

  override fun observeModelStore(): Observable<ModelStoreChangeEvent> {
    return eventSubject
        .filter { it is ModelStoreChangeEvent }
        .map { it as ModelStoreChangeEvent }
  }

  override fun observeModel(modelId: ModelId): Observable<PropertyChangeEvent> {
    return eventStreamAsPropertyChangeEvent()
        .filter { it.modelId == modelId }
  }

  override fun observeModel(modelType: ModelType): Observable<PropertyChangeEvent> {
    return eventStreamAsPropertyChangeEvent()
        .filter { it.modelType == modelType }
  }

  override fun observeProperty(modelId: ModelId, propertyName: PropertyName): Observable<ValueChangeEvent> {
    return eventStreamAsPropertyChangeEvent()
        .filter { it.modelId == modelId }
        .filter { it.valueChangeEvent.property == propertyName }
        .map { it.valueChangeEvent }
  }

  private fun eventStreamAsPropertyChangeEvent(): Observable<PropertyChangeEvent> {
    return eventSubject
        .filter { it is PropertyChangeEvent }
        .map { it as PropertyChangeEvent }
  }

  override fun registerNamedCommand(actionName: String): Observable<ActionEvent> {
    return eventSubject
        .filter { it is ActionEvent }
        .map { it as ActionEvent }
        .filter { it.actionName == actionName }
  }

  override fun models(modelType: ModelType): Collection<PresentationModel> {
    return modelStore[modelType]
  }

  override fun model(modelId: ModelId): PresentationModel {
    return modelStore[modelId]
  }

  override fun contains(modelId: ModelId): Boolean {
    return modelStore.contains(modelId)
  }

  override fun contains(modelType: ModelType): Boolean {
    return modelStore.contains(modelType)
  }

  override fun getAllModels(): Collection<PresentationModel> {
    return modelStore.getAllModels()
  }

}

