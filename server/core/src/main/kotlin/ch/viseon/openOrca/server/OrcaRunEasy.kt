/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.server

import ch.viseon.openOrca.server.impl.CommandListExecutor
import ch.viseon.openOrca.server.impl.OrcaSourceImpl
import ch.viseon.openOrca.server.impl.loggerFor
import ch.viseon.openOrca.share.CommandData
import ch.viseon.openOrca.share.Event
import ch.viseon.openOrca.share.ModelStore
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import java.util.logging.Level


class OrcaRunEasy(
        private val commandListExecutor: CommandListExecutor,
        private val requestProcessor: (OrcaSource) -> Observable<CommandData>,
        private val storeLifecycle: StoreLifecycle) {

  companion object {
    @JvmStatic
    val LOGGER = loggerFor<OrcaRun>()
  }

  fun execute(request: Request, responseProcessor: Response) {
    val modelStore = storeLifecycle.getStore()

    val initialCommands = request.readCommands().toList().blockingGet()
    var result = execute(modelStore, initialCommands)
    val toSend = PublishSubject.create<CommandData>()
    while(result.isNotEmpty())  {
      responseProcessor.sendCommands(toSend)
      result.forEach(toSend::onNext)
      result = execute(modelStore, result)
    }
    toSend.onComplete()
    storeLifecycle.updateStore(modelStore)
  }

  //Für jeden "Batch" an Events wird eine Registrierung aller mappings durchgeführt.
  //Kann diese Registrierung übernommen werden?
  private fun execute(modelStore: ModelStore, commands: List<CommandData>): List<CommandData> {
    val eventSubject = ReplaySubject.create<Event>()
    val orcaSource = OrcaSourceImpl(modelStore, eventSubject)

    val responseCommands = requestProcessor(orcaSource)

    //Das ist eine separates Observable. Nur durch die Eingabe des Events an das EventSubject werden die
    //Daten in die Maschinere gespiesen.
    val inputCommands = Observable.fromIterable(commands)
    commandListExecutor.execute(modelStore, inputCommands)
            .doOnError { LOGGER.log(Level.SEVERE, "Error during command processing", it) }
            .doOnComplete(eventSubject::onComplete)
            .filter{ it.applied }
            .map { it.events }
            .subscribe {
                it.forEach(eventSubject::onNext)
            }


    return responseCommands.toList().blockingGet()
  }

}
