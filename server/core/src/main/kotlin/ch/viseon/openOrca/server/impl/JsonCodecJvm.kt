/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.server.impl

import ch.viseon.openOrca.share.JsonArray
import ch.viseon.openOrca.share.JsonFactory
import ch.viseon.openOrca.share.JsonObj
import com.beust.klaxon.*
import java.io.StringReader


class JsonFactoryJvm: JsonFactory {

  val parser = Parser()

  override fun newJson(block: JsonObj.() -> Unit): JsonObj {
    val jsonObjJvm = JsonObjJvm(JsonObject())
    jsonObjJvm.block()
    return jsonObjJvm
  }

  @Suppress("UNCHECKED_CAST")
  override fun parse(content: String): JsonArray<JsonObj> {
    val parse = parser.parse(StringReader(content))!! as com.beust.klaxon.JsonArray<JsonObject>
    return JsonArray(parse.map(::JsonObjJvm))
  }
}

class JsonObjJvm(private val jsonObject: JsonObject): JsonObj {


  override fun set(key: String, value: String) {
    jsonObject[key] = value
  }

  override fun set(key: String, value: Boolean) {
    jsonObject[key] = value
  }

  override fun set(key: String, value: Array<String>) {
    jsonObject[key] = value
  }

  override fun set(key: String, block: JsonObj.() -> Unit) {
    val newJsonObj = JsonObjJvm(JsonObject())
    newJsonObj.block()
    jsonObject[key] = newJsonObj.jsonObject
  }

  override fun set(key: String, value: JsonObj) {
    jsonObject[key] = (value as JsonObjJvm).jsonObject
  }

  override fun set(key: String, value: JsonArray<JsonObj>) {
    jsonObject[key] = com.beust.klaxon.JsonArray(value.map { (it as JsonObjJvm).jsonObject })
  }

  override fun set(key: String, value: Iterable<String>) {
    jsonObject[key] = value
  }

  override fun string(key: String): String {
    return jsonObject.string(key)!!
  }

  override fun optionalString(key: String): String? {
    return jsonObject.string(key)
  }

  override fun boolean(key: String): Boolean {
    return jsonObject.string(key)!!.toBoolean()
  }

  override fun optionalBoolean(key: String): Boolean? {
    return jsonObject.boolean(key)
  }

  override fun getObject(key: String): JsonObj {
    return JsonObjJvm(jsonObject.obj(key)!!)
  }

  override fun <T> pArray(key: String): JsonArray<T> {
    return JsonArray(jsonObject.array<T>(key)!!)
  }


  override fun oArray(key: String): JsonArray<JsonObj> {
    return JsonArray(jsonObject.array<JsonObject>(key)!!.map(::JsonObjJvm))
  }

  override fun toJsonString(): String {
    return jsonObject.toJsonString(false)
  }

  override fun integer(key: String): Int {
    return jsonObject.string(key)!!.toInt()
  }

  override fun long(key: String): Long {
    return jsonObject.string(key)!!.toLong()
  }

  override fun float(key: String): Float {
    return jsonObject.string(key)!!.toFloat()
  }

  override fun double(key: String): Double {
    return jsonObject.string(key)!!.toDouble()
  }

}

