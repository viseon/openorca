/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.server.impl.ServerCommandListExecutor
import ch.viseon.openOrca.share.Event
import ch.viseon.openOrca.share.ModelStoreChangeEvent
import ch.viseon.openOrca.share.SharedTestData
import ch.viseon.openOrca.share.Source
import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CreateModelServerCommandTest {

  lateinit private var modelStore: DefaultPresentationModelStore

  @Before
  fun setUp() {
    modelStore = DefaultPresentationModelStore()
  }

  @Test
  fun `create model`() {
    val builder = SharedTestData.createPmWithOneProperty()
    val commandExecutor = ServerCommandListExecutor()
    val event = commandExecutor.execute(modelStore, builder.build(Source.REQUEST))
            .flatMap { Observable.fromIterable(it.events) }
            .map { it: Event -> it as ModelStoreChangeEvent }
            .blockingFirst()

    assertEquals(SharedTestData.modelId, event.modelId)
    val model = modelStore.get(SharedTestData.modelId)
    assertEquals(SharedTestData.modelId, model.id)
    val property1 = model.getProperties().first()
    assertEquals(SharedTestData.prop1, property1.name)
  }

}

