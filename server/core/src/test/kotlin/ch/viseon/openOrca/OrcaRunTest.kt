/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.server.*
import ch.viseon.openOrca.server.impl.ServerCommandListExecutor
import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.impl.DefaultPresentationModel
import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import io.reactivex.Observable
import org.junit.Assert.*
import org.junit.Before
import org.junit.Ignore
import org.junit.Test

class OrcaRunTest {

  lateinit var storeLifecycle: TestStoreLifecycle

  @Before
  fun setUp() {
    storeLifecycle = TestStoreLifecycle()
  }


  @Ignore
  @Test
  fun `process single event`() {
    val command = ChangeValueCommandData(Source.RESPONSE,
        SharedTestData.modelId,
        SharedTestData.prop1,
        Tag.VALUE,
        "seen"
    )

    fun handler(source: OrcaSource): Observable<CommandData> {
      return source
          .observeModelStore()
          .filter { it.modelId == SharedTestData.modelId }
          .flatMap { Observable.just(command) }
    }

    storeLifecycle.assertStore {
      val prop1 = (it[SharedTestData.modelId] as DefaultPresentationModel)[SharedTestData.prop1]
      assertEquals("seen", (prop1[Tag.VALUE] as String))
    }

    execute(
        listOf(createSimpleModel().build(Source.REQUEST)),
        setOf(command),
        ::handler)
  }

  @Test
  fun `process multiple Event with back feed`() {
    val changeValueCommand = ChangeValueCommandData(Source.RESPONSE, SharedTestData.modelId, SharedTestData.prop1, Tag.VALUE, "seen")
    val changeLabelCommand = ChangeValueCommandData(Source.RESPONSE, SharedTestData.modelId, SharedTestData.prop1, Tag.LABEL, "A new label")

    fun handler(source: OrcaSource): Observable<CommandData> {
      val `createModel$` = source
          .observeModelStore()
          .filter { it.modelId == SharedTestData.modelId }
          .flatMap { Observable.just(changeValueCommand) }

      val `valueChanged$` = source
          .observeProperty(SharedTestData.modelId, SharedTestData.prop1)
          .filter { it.isValueChange() }
          .flatMap { Observable.just(changeLabelCommand) }

      return Observable.merge(`createModel$`, `valueChanged$`)
    }

    storeLifecycle.assertStore {
      val model = it[SharedTestData.modelId] as DefaultPresentationModel
      val prop1 = model[SharedTestData.prop1]

      assertEquals(changeLabelCommand.value, prop1.getValue(Tag.LABEL))
      assertEquals(changeValueCommand.value, prop1.getValue())
    }

    execute(
        listOf(createSimpleModel().build(Source.REQUEST)),
        setOf(changeValueCommand, changeLabelCommand),
        ::handler)
  }

  @Test
  fun dontSendCommandsWhichAreNotApplied() {
    val command = createSimpleModel().build(Source.RESPONSE)

    fun handler(source: OrcaSource): Observable<CommandData> {
      return source
          .observeModelStore()
          .filter { it.source.isRequest() }
          .filter { it.modelId == SharedTestData.modelId }
          .flatMap { Observable.just(command) }
    }

    execute(
        listOf(createSimpleModel().build(Source.REQUEST)),
        setOf(), //command should be executed, since its application has no effect
        ::handler)
  }


  @Test
  fun forceSendEvenIfNotApplied() {
    val command = createSimpleModel().build(Source.RESPONSE, ApplyPolicy.FORCE_SEND)

    fun handler(source: OrcaSource): Observable<CommandData> {
      return source
          .observeModelStore()
          .filter { it.source.isRequest() }
          .filter { it.modelId == SharedTestData.modelId }
          .flatMap { Observable.just(command) }
    }

    execute(
        listOf(createSimpleModel().build(Source.REQUEST)),
        setOf(command), //command should be executed, since its application has no effect
        ::handler)
  }

  private fun execute(requestCommands: Iterable<CommandData>, expectedCommands: Set<CommandData>, processor: (OrcaSource) -> Observable<CommandData>) {
    val response = ResponseHandler(expectedCommands)
    val orcaSetup = OrcaRun(ServerCommandListExecutor(), processor, storeLifecycle)
    orcaSetup.execute(TestRequest(requestCommands), response)
    response.assertExecution()
  }

  private fun createSimpleModel(): PresentationModelBuilder {
    return PresentationModelBuilder(SharedTestData.modelId, SharedTestData.modelType) {
      property(SharedTestData.prop1) {
        label = "Hallo Welt"
      }
    }
  }

}

class TestStoreLifecycle : StoreLifecycle {

  val modelStore = DefaultPresentationModelStore()
  private var assertBlock: (ModelStore) -> Unit = {}

  override fun getStore(): DefaultPresentationModelStore {
    return modelStore
  }

  fun assertStore(block: (ModelStore) -> Unit) {
    assertBlock = block
  }

  override fun updateStore(modelStore: DefaultPresentationModelStore) {
    assertBlock(modelStore)
  }

}

private class ResponseHandler(val expectedCommands: Set<CommandData>) : Response {

  var error: Throwable? = null
  var counter = 0
  var onCompleted = false

  override fun sendCommands(commands: Observable<CommandData>) {
    commands
        .subscribe({
          assertTrue("Command not expected: $it", expectedCommands.contains(it))
          ++counter
          if (counter > expectedCommands.size) {
            fail("To many executions")
          }
        },
            { error = it }, //no error handling
            {
              println("OnComplete called")
              onCompleted = true
            })
  }

  fun assertExecution() {
    error?.let { it.printStackTrace(); fail("Error received") }
    assertTrue("OnComplete not fired", onCompleted)
    assertEquals(expectedCommands.size, counter)
  }

}

private class TestRequest(val command: Iterable<CommandData>) : Request {

  override fun readCommands(): Observable<CommandData> {
    return Observable.fromIterable(command)
  }

}

