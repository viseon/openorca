/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.server.impl.JsonFactoryJvm
import ch.viseon.openOrca.share.AbstractJsonCodecTest
import ch.viseon.openOrca.share.JsonFactory
import org.junit.Test


class JsonCodecTest : AbstractJsonCodecTest() {

  @Test
  override fun decodeAndEncodeActionCommandData() {
    super.decodeAndEncodeActionCommandData()
  }

  @Test
  override fun decodeAndEncodeRemoveModelCommandData() {
    super.decodeAndEncodeRemoveModelCommandData()
  }

  @Test
  override fun decodeChangeValueCommandData() {
    super.decodeChangeValueCommandData()
  }

  @Test
  override fun decodeAndEncodeChangeValueCommandDataWithTagValue() {
    super.decodeAndEncodeChangeValueCommandDataWithTagValue()
  }

  @Test
  override fun decodeAndEncodeChangeValueCommandWithIntegerValue() {
    super.decodeAndEncodeChangeValueCommandWithIntegerValue()
  }

  @Test
  override fun decodeCreateCommandData() {
    super.decodeCreateCommandData()
  }

  @Test
  override fun decodeAndEncodeRemoveByTypeCommandData() {
    super.decodeAndEncodeRemoveByTypeCommandData()
  }

  @Test
  override fun decodeAndEncodeSyncModelCommandData() {
    super.decodeAndEncodeSyncModelCommandData()
  }

  @Test
  override fun decodeAndEncodeAForcedCommand() {
    super.decodeAndEncodeAForcedCommand()
  }

  @Test
  override fun decodeEncodeDoubleValue() {
    super.decodeEncodeDoubleValue()
  }

  @Test
  override fun decodeEncodeFloatValue() {
    super.decodeEncodeFloatValue()
  }

  @Test
  override fun decodeEncodeIntValue() {
    super.decodeEncodeIntValue()
  }

  @Test
  override fun decodeEncodeStringValue() {
    super.decodeEncodeStringValue()
  }

  @Test
  override fun decodeEncodeModelIdValue() {
    super.decodeEncodeModelIdValue()
  }

  @Test
  override fun decodeAndEncodeBooleanValue() {
    super.decodeAndEncodeBooleanValue()
  }

  @Test
  override fun decodeAndEncodeLongValue() {
    super.decodeAndEncodeLongValue()
  }

  @Test
  override fun decodeAndEncodeModelType() {
    super.decodeAndEncodeModelType()
  }

  @Test
  override fun encodeFaultyCommand() {
    super.encodeFaultyCommand()
  }

  @Test
  override fun decodeAndEncodeCompositeCommand() {
    super.decodeAndEncodeCompositeCommand()
  }

  override fun createJsonFactory(): JsonFactory {
    return JsonFactoryJvm()
  }

}
