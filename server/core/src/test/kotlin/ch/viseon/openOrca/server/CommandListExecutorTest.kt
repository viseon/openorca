/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.server

import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class CommandListExecutorTest : AbstractCommandApplierTest() {

  @Test
  override fun syncWithModel() {
    super.syncWithModel()
  }

  @Test
  override fun changeValueCommandIsApplied() {
    super.changeValueCommandIsApplied()
  }

  @Test
  override fun changeValueTwiceDoesNotGenerateASecondEvent() {
    super.changeValueTwiceDoesNotGenerateASecondEvent()
  }

  @Test
  override fun removeModelApplyTwiceIsNotApplied() {
    super.removeModelApplyTwiceIsNotApplied()
  }

  @Test
  override fun removeModelByType() {
    super.removeModelByType()
  }

  @Test
  override fun removeModelByTypeApplyTwiceIsNotApplied() {
    super.removeModelByTypeApplyTwiceIsNotApplied()
  }

  @Test
  override fun createModelApplyTwiceIsNotApplied() {
    super.createModelApplyTwiceIsNotApplied()
  }

  @Test
  override fun applyCommandWithDifferentSourceIfItIsForceSend() {
    super.applyCommandWithDifferentSourceIfItIsForceSend()
  }

  @Test
  override fun forceApplyChangeValueCommand() {
    super.forceApplyChangeValueCommand()
  }

  @Test
  override fun syncValueCommand() {
    super.syncValueCommand()
  }

  @Test
  override fun decorateExceptionIfCommandApplicationFails() {
    super.decorateExceptionIfCommandApplicationFails()
  }

  @Test
  override fun compositeCommand() {
    super.compositeCommand()
  }

  @Test
  fun changeValueForClientModel() {
    //Setup
    val modelStore = SharedTestData.createModelStore(Source.RESPONSE, SharedTestData.createClientPm(Source.RESPONSE))

    //Execution
    CommandApplier.apply(Source.RESPONSE,
            modelStore,
            ChangeValueCommandData(
                    Source.RESPONSE,
                    SharedTestData.clientModelId,
                    SharedTestData.clientProperty,
                    Tag.LABEL,
                    "NewValue"
            ))

    //Assertion
    assertEquals(0, modelStore[SharedTestData.clientModelId].propertyCount()) //Property is still not present, and not exception occurred
  }

  @Test
  fun changeValueForClientProperty() {
    //Setup
    val modelStore = SharedTestData.createModelStore(Source.RESPONSE, SharedTestData.createPmWithClientProperty(Source.RESPONSE))

    //Execution
    try {
      CommandApplier.apply(Source.RESPONSE,
              modelStore,
              ChangeValueCommandData(
                      Source.RESPONSE,
                      SharedTestData.clientModelId,
                      SharedTestData.clientProperty,
                      Tag.LABEL,
                      "NewValue"
              ))
      fail("ChangeValue for a client property is not possible")
    } catch (e: Throwable) {
      //Application should not be possible
      assertTrue(e.cause is IllegalArgumentException)
    }
  }

  @Test
  fun changeValueForClientTag() {
    //Setup
    val modelStore = SharedTestData.createModelStore(Source.RESPONSE, SharedTestData.createPmWithClientTag(Source.RESPONSE))

    //Execution
    val commandApplication = CommandApplier.apply(Source.RESPONSE,
            modelStore,
            ChangeValueCommandData(Source.RESPONSE, SharedTestData.clientModelId, SharedTestData.clientProperty, Tag.LABEL, "NewValue"))


    //Assertion
    assertFalse(commandApplication.applied, "client command should not be executed on the controller side")
    assertTrue(commandApplication.sendCommand(), "client command must be sent to the client")
    assertTrue(commandApplication.events.count() == 0, "No events expected since the command was not applied")

    assertFalse(modelStore[SharedTestData.clientModelId][SharedTestData.clientProperty].hasValue(Tag.LABEL))
  }

  @Test
  fun createClientPm() {
    //Setup
    val modelStore = DefaultPresentationModelStore()

    //Execution
    val commandApplication = CommandApplier.apply(Source.RESPONSE,
            modelStore,
            SharedTestData.createClientPm(Source.RESPONSE))


    //Assertion
    assertFalse(commandApplication.applied, "client command should not be executed on the controller side")
    assertTrue(commandApplication.sendCommand(), "client command must be sent to the client")
    assertTrue(commandApplication.events.count() == 0, "No events expected since the command was not applied")

    assertTrue(modelStore.contains(SharedTestData.clientModelId), "client pm must be in store")
    assertTrue(modelStore[SharedTestData.clientModelId].clientOnly, "Pm should be marked as a client onl pm")
    assertEquals(0, modelStore[SharedTestData.clientModelId].propertyCount())
  }
}
