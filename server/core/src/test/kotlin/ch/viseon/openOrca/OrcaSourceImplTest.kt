/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.server.impl.OrcaSourceImpl
import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import kotlin.test.fail

class OrcaSourceImplTest {

  private val modelStore = DefaultPresentationModelStore()
  private val subject = PublishSubject.create<Event>()
  private val sourceImpl = OrcaSourceImpl(modelStore, subject)

  @Before
  fun setUp() {

  }


  @Test
  fun `observer property`() {
    var passed = false
    sourceImpl
            .observeProperty(SharedTestData.modelId, SharedTestData.prop1)
            .subscribe {
              passed = true
            }

    subject.onNext(generateChangeValueEvent(SharedTestData.prop1))

    assertTrue(passed)
  }

  @Test
  fun `observe property is notified for correct property`() {
    var isEmpty = false
    sourceImpl
            .observeProperty(SharedTestData.modelId, SharedTestData.prop2)
            .isEmpty
            .subscribe({
              isEmpty = it
            }, {
              fail("Error $it")
            })

    subject.onNext(generateChangeValueEvent(SharedTestData.prop1))
    subject.onComplete()
    assertTrue("Event emitted for non observed property", isEmpty)
  }

  private fun generateChangeValueEvent(propertyName: PropertyName): PropertyChangeEvent {
    return PropertyChangeEvent(
            Source.RESPONSE,
            false,
            true,
            SharedTestData.modelId,
            SharedTestData.modelType,
            ValueChangeEvent(
                    Source.RESPONSE,
                    false,
                    true,
                    propertyName,
                    Tag.VALUE,
                    null,
                    "New Value"
            )
    )
  }
}
