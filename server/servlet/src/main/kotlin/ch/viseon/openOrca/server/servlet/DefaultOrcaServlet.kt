/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.server.servlet

import ch.viseon.openOrca.server.*
import ch.viseon.openOrca.server.impl.JsonFactoryJvm
import ch.viseon.openOrca.server.impl.ServerCommandListExecutor
import ch.viseon.openOrca.server.impl.loggerFor
import ch.viseon.openOrca.share.CommandData
import ch.viseon.openOrca.share.JsonCodec
import ch.viseon.openOrca.share.Source
import ch.viseon.openOrca.share.StringCodec
import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import io.reactivex.Observable
import java.io.ByteArrayOutputStream
import java.io.OutputStreamWriter
import java.util.logging.Level
import javax.servlet.http.*

abstract class DefaultOrcaServlet(private val configuration: Configuration) : HttpServlet() {

  override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
    val run = OrcaRun(ServerCommandListExecutor(), this::processRequest, ServletStoreLifeCycle(req, resp))

    if (configuration.crossOriginHeader.isNotEmpty()) {
      resp.addHeader("Access-Control-Allow-Origin", configuration.crossOriginHeader)
      resp.addHeader("Access-Control-Allow-Credentials", "true")
    }
    run.execute(ServletRequest(req, configuration.codec), ServletResponse(resp, configuration.codec))
  }

  protected abstract fun processRequest(orcaSource: OrcaSource): Observable<CommandData>

}

private class ServletResponse(private val resp: HttpServletResponse, private val codec: StringCodec) : Response {

  companion object {
    @JvmStatic
    val LOGGER = loggerFor<ServletResponse>()
  }

  override fun sendCommands(commands: Observable<CommandData>) {
    val toSend = mutableListOf<CommandData>()
    commands.subscribe({
      toSend.add(it)
    },
            { LOGGER.log(Level.SEVERE, "Error during send", it) },
            {
              val json = codec.encode(toSend)
              resp.status = 200
              resp.characterEncoding = codec.encoding
              val bos = ByteArrayOutputStream()
              OutputStreamWriter(bos, codec.encoding)
                      .use {
                        LOGGER.fine { "send json: $json" }
                        it
                                .append(json)
                                .append('\n')
                      }
              resp.setHeader("Content-Length", bos.size().toString())
              resp.setHeader("Content-Type", "application/json; charset=${codec.encoding.toLowerCase()}")
              resp.outputStream.use {
                it.write(bos.toByteArray())
              }
            })
  }

}

private class ServletRequest(private val req: HttpServletRequest, private val codec: StringCodec) : Request {

  override fun readCommands(): Observable<CommandData> {
    val commands = req.reader.useLines {
      codec.decode(Source.REQUEST, it.joinToString(""))
    }
    return Observable.fromIterable(commands)
  }

}

class ServletStoreLifeCycle(private val req: HttpServletRequest, private val resp: HttpServletResponse) : StoreLifecycle {

  companion object {
    @JvmStatic
    val MODEL_STORE = "modelStore"
  }

  override fun getStore(): DefaultPresentationModelStore {
    val session = getSession()
    return session.getAttribute(MODEL_STORE)
            ?.let { it as DefaultPresentationModelStore }
            ?: DefaultPresentationModelStore()
                    .also {
                      session.setAttribute(MODEL_STORE, it)
                    }
  }

  override fun updateStore(modelStore: DefaultPresentationModelStore) {
    getSession().setAttribute(MODEL_STORE, modelStore)
  }

  private fun getSession(): HttpSession {
    //Force session cookie in cases, where the server/browser don't want to create one
    val session = req.getSession(true)
    if (req.getParameter("JSESSIONID") != null) {
      val userCookie = Cookie("JSESSIONID", req.getParameter("JSESSIONID"))
      resp.addCookie(userCookie)
    } else {
      val sessionId = session.getId()
      val userCookie = Cookie("JSESSIONID", sessionId)
      resp.addCookie(userCookie)
    }
    return session
  }

}

data class Configuration(val crossOriginHeader: String, val codec: StringCodec = JsonCodec(JsonFactoryJvm()))

