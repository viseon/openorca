/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

abstract class AbstractCommandApplierTest {

  open fun syncWithModel() {
    //Setup
    val modelStore = SharedTestData.createModelStoreWithTwoModels()
    val value = "New Value"
    modelStore[SharedTestData.modelId][SharedTestData.prop1][Tag.VALUE] = value

    val syncCommandData = SyncModelCommandData(Source.REQUEST, SharedTestData.modelId, SharedTestData.modelId2)
    //Execution
    CommandApplier.apply(Source.REQUEST, modelStore, syncCommandData)

    //Assertion
    val destModel = modelStore[SharedTestData.modelId2]
    val newValue: String = destModel[SharedTestData.prop1].getValue()
    assertEquals(value, newValue)
  }

  open fun changeValueCommandIsApplied() {
    //setup
    val modelStore = SharedTestData.createModelStoreWithTwoModels()

    //execution
    val events = CommandApplier.apply(Source.REQUEST, modelStore,
            ChangeValueCommandData(
                    Source.REQUEST,
                    SharedTestData.modelId,
                    SharedTestData.prop1,
                    Tag.VALUE,
                    "1"
            )).events

    //Assertion
    assertEquals(1, events.count())
  }

  open fun changeValueTwiceDoesNotGenerateASecondEvent() {
    //setup
    val modelStore = SharedTestData.createModelStoreWithTwoModels()

    val command = ChangeValueCommandData(Source.REQUEST, SharedTestData.modelId, SharedTestData.prop1, Tag.VALUE, "1")

    //execution & assertion
    assertDoubleApplication(modelStore, command)
  }

  open fun removeModelApplyTwiceIsNotApplied() {
    //setup
    val modelStore = SharedTestData.createModelStoreWithTwoModels()

    val command = RemoveModelCommandData(Source.REQUEST, SharedTestData.modelId, SharedTestData.modelType)

    //execution & assertion
    assertDoubleApplication(modelStore, command)
  }

  open fun removeModelByType() {
    //setup
    val modelStore = SharedTestData.createModelStoreWithTwoModels()
    val command = RemoveModelByTypeCommandData(Source.REQUEST, SharedTestData.modelType)

    //Execution
    val application = CommandApplier.apply(Source.REQUEST, modelStore, command)

    //Assertion
    assertEquals(1, application.events.count())
    val event = application.events.first()
    assertTrue(event.isCompositeUpdate, "RemoveModel is a compslite update")
    assertTrue(event.isLastEventOfCompositeUpdate, "Only one model should be remove")
  }

  open fun removeModelByTypeApplyTwiceIsNotApplied() {
    //setup
    val modelStore = SharedTestData.createModelStoreWithTwoModels()

    val command = RemoveModelByTypeCommandData(Source.REQUEST, SharedTestData.modelType)

    //execution & assertion
    assertDoubleApplication(modelStore, command)
  }

  open fun createModelApplyTwiceIsNotApplied() {
    //setup
    val modelStore = DefaultPresentationModelStore()

    //execution
    val command = SharedTestData.createModelCommand()

    val finalEventList = combineEvents(
            CommandApplier.apply(Source.REQUEST, modelStore, command).events,
            CommandApplier.apply(Source.REQUEST, modelStore, command).events
    )

    //Assertion
    assertEquals(1, finalEventList.size)
  }

  open fun applyCommandWithDifferentSourceIfItIsForceSend() {
    //setup
    val modelStore = DefaultPresentationModelStore()

    //execution
    val createModelCommandData = SharedTestData.createModelCommand()
    val changeValueCommand = SharedTestData.changeValueCommand(policy = ApplyPolicy.FORCE_SEND)

    val finalEventList = combineEvents(
            CommandApplier.apply(Source.RESPONSE, modelStore, createModelCommandData).events,
            CommandApplier.apply(Source.RESPONSE, modelStore, changeValueCommand).events,
            CommandApplier.apply(Source.RESPONSE, modelStore, changeValueCommand).events
    )

    //Assertion
    assertEquals(3, finalEventList.size)
  }

  open fun forceApplyChangeValueCommand() {
    //setup
    val modelStore = SharedTestData.createModelStoreWithOneModel()

    val changeCommand = ChangeValueCommandData(Source.RESPONSE, SharedTestData.modelId, SharedTestData.prop1, Tag.VALUE, "1", ApplyPolicy.FORCE_APPLY)

    //execution
    val finalList = combineEvents(
            CommandApplier.apply(Source.RESPONSE, modelStore, changeCommand).events,
            CommandApplier.apply(Source.RESPONSE, modelStore, changeCommand).events
    )

    assertEquals(2, finalList.size)
  }

  open fun syncValueCommand() {
    //setup
    val expectedValue = "Test value for Property"
    val modelStore = DefaultPresentationModelStore().also {
      val testModel = PresentationModelBuilder(SharedTestData.modelId, SharedTestData.modelType) {
        property(SharedTestData.prop1) {
          label = expectedValue
        }
        property(SharedTestData.prop2) {
          label = SharedTestData.prop2.name
        }
      }
      it.addModel(SharedTestData.createModel(testModel))
      it.addModel(SharedTestData.createModel(SharedTestData.createPmWithOneProperty(SharedTestData.modelType2, SharedTestData.modelId2)))
    }

    val syncCommand = SyncModelCommandData(Source.REQUEST, SharedTestData.modelId, SharedTestData.modelId2)

    //execution
    CommandApplier.apply(Source.REQUEST, modelStore, syncCommand)
            //Assertion
            .also {
              assertTrue(it.applied)
              assertEquals(1, it.events.count())
              assertEquals(syncCommand, it.commandData)
            }


    val actualValue = modelStore[SharedTestData.modelId2][SharedTestData.prop1].getValue<String>(Tag.LABEL)
    assertEquals(expectedValue, actualValue)
  }

  open fun decorateExceptionIfCommandApplicationFails() {
    val modelStore = DefaultPresentationModelStore()
    val changeCommand = ChangeValueCommandData(Source.RESPONSE, ModelId("NOT_EXISTING_MODEL"), SharedTestData.prop1, Tag.VALUE, "1")
    try {
      CommandApplier.apply(Source.REQUEST, modelStore, changeCommand)
    } catch (e: Throwable) {
      assertTrue(e.message!!.contains("command: '$changeCommand"), "Exception should contains offending command")
    }
  }

  open fun compositeCommand() {
    val modelStore = SharedTestData.createModelStore(
            Source.REQUEST,
            SharedTestData.createPmWithTwoProperties().build(Source.REQUEST)
    )

    val command = CompositeCommandData(Source.REQUEST,
            listOf(
                    SharedTestData.changeValueCommand(Source.REQUEST),
                    SharedTestData.changeValueCommand2(Source.REQUEST)
            )
    )


    val applications = CommandApplier.apply(Source.REQUEST, modelStore, command)
    assertTrue(applications.applied)
    val expectedEvents = listOf(
            SharedTestData.propertyChangeEvent1(isCompositeEvent = true, isLastOfCompositeUpdate = false),
            SharedTestData.propertyChangeEvent2(isCompositeEvent = true, isLastOfCompositeUpdate = true)
    )

    assertEquals(expectedEvents.size, applications.events.count())
    applications.events.forEachIndexed { index, event -> assertEquals(expectedEvents[index], event) }
  }

  private fun assertDoubleApplication(modelStore: ModelStore, commandData: CommandData) {
    val application1 = CommandApplier.apply(Source.REQUEST, modelStore, commandData)
    val application2 = CommandApplier.apply(Source.REQUEST, modelStore, commandData)

    val finalEventList = combineEvents(application1.events, application2.events)

    //Assertion
    assertFalse(application2.applied, "Command ${commandData::class.simpleName!!} should not be applied.")
    assertEquals(1, finalEventList.size)
  }

  private fun combineEvents(vararg lists: Iterable<Event>): List<Event> {
    val result = mutableListOf<Event>()
    lists.forEach { result.addAll(it) }
    return result
  }
}

