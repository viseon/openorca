/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

import ch.viseon.openOrca.share.impl.DefaultPresentationModel
import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore

object SharedTestData {

  val modelId = ModelId("modelId")
  val modelId2 = ModelId("modelId2")

  val modelType = ModelType("modelType")
  val modelType2 = ModelType("modelType2")

  val prop1 = PropertyName("1")
  val prop2 = PropertyName("2")

  val clientModelId = ModelId("ClientPm")
  val clientPmType = ModelType("ClientPm")
  val clientProperty = PropertyName("width")

  fun createPmWithOneProperty(): ModelBuilder {
    return createPmWithOneProperty(modelType, modelId)
  }

  fun createPmWithTwoProperties(): ModelBuilder {
    return createPmWithTwoProperties(modelType, modelId)
  }

  fun createPm2WithOneProperty(): ModelBuilder {
    return createPmWithOneProperty(modelType2, modelId2)
  }

  fun createPmWithOneProperty(modelType: ModelType, modelId: ModelId): ModelBuilder {
    return PresentationModelBuilder(modelId, modelType) {
      property(prop1) {
        label = prop1.name
      }
    }
  }

  fun createPmWithTwoProperties(modelType: ModelType, modelId: ModelId): ModelBuilder {
    return PresentationModelBuilder(modelId, modelType) {
      property(prop1) {
        label = prop1.name
      }
      property(prop2) {
        label = prop2.name
      }
    }
  }

  fun actionCommand(): ActionCommandData {
    return ActionCommandData(Source.REQUEST, "action1", listOf(modelId, modelId))
  }

  fun changeValueCommand(source: Source = Source.REQUEST, policy: ApplyPolicy = ApplyPolicy.DEFAULT): ChangeValueCommandData {
    return ChangeValueCommandData(source, modelId, prop1, Tag.LABEL, "A new value", policy)
  }

  fun propertyChangeEvent1(source: Source = Source.REQUEST, isCompositeEvent: Boolean = false, isLastOfCompositeUpdate: Boolean = true): PropertyChangeEvent {
    return PropertyChangeEvent(
            source,
            isCompositeEvent,
            isLastOfCompositeUpdate,
            SharedTestData.modelId,
            SharedTestData.modelType,
            ValueChangeEvent(
                    source,
                    isCompositeEvent,
                    isLastOfCompositeUpdate,
                    SharedTestData.prop1,
                    Tag.LABEL,
                    "1",
                    "A new value"
            )
    )
  }

  fun changeValueCommand2(source: Source = Source.REQUEST, policy: ApplyPolicy = ApplyPolicy.DEFAULT): ChangeValueCommandData {
    return ChangeValueCommandData(source, modelId, prop2, Tag.LABEL, "A new value", policy)
  }

  fun propertyChangeEvent2(source: Source = Source.REQUEST, isCompositeEvent: Boolean = false, isLastOfCompositeUpdate: Boolean = true): PropertyChangeEvent {
    return PropertyChangeEvent(
            source,
            isCompositeEvent,
            isLastOfCompositeUpdate,
            SharedTestData.modelId,
            SharedTestData.modelType,
            ValueChangeEvent(
                    source,
                    isCompositeEvent,
                    isLastOfCompositeUpdate,
                    SharedTestData.prop2,
                    Tag.LABEL,
                    "2",
                    "A new value"
            )
    )
  }

  fun createModelCommand(modelBuilder: ModelBuilder): CreateModelCommandData {
    return modelBuilder.build(Source.REQUEST)
  }

  fun createModelCommand(): CreateModelCommandData {
    return createModelCommand(createPmWithOneProperty())
  }

  fun removeModelCommand(): CommandData {
    return RemoveModelCommandData(Source.REQUEST, modelId, modelType)
  }

  fun createModel(it: CreateModelCommandData): DefaultPresentationModel {
    return DefaultPresentationModel(it.modelId, it.modelType, false, it.properties.asSequence())
  }

  fun createModel(it: ModelBuilder): DefaultPresentationModel {
    return createModel(it.build(Source.REQUEST))
  }

  fun createModelStoreWithTwoModels(): ModelStore {
    return DefaultPresentationModelStore().also {
      it.addModel(createModel(createPmWithOneProperty()))
      it.addModel(createModel(createPm2WithOneProperty()))
    }
  }

  fun createModelStoreWithOneModel(): ModelStore {
    return DefaultPresentationModelStore().also {
      it.addModel(createModel(createPmWithOneProperty()))
    }
  }

  fun createModelStore(source: Source, vararg commands: CreateModelCommandData): ModelStore {
    return DefaultPresentationModelStore().also { modelStore ->
      commands.forEach { CommandApplier.apply(source, modelStore, it) }
    }
  }

  fun removeModelByTypeCommand(): RemoveModelByTypeCommandData {
    return RemoveModelByTypeCommandData(Source.REQUEST, modelType)
  }

  fun syncModelCommand(): SyncModelCommandData {
    return SyncModelCommandData(Source.REQUEST, modelId, modelId2)
  }

  fun compositeCommand(): CompositeCommandData {
    return CompositeCommandData(Source.REQUEST,
            listOf(
//                    actionCommand(),
//                    syncModelCommand(),
//                    removeModelCommand(),
//                    removeModelByTypeCommand(),
//                    changeValueCommand()
                    createModelCommand()
            )
    )
  }

  fun createClientPm(source: Source): CreateModelCommandData {
    return createClientPmHelper(source, true, false, false)
  }

  fun createPmWithClientProperty(source: Source): CreateModelCommandData {
    return createClientPmHelper(source, false, true, false)
  }

  fun createPmWithClientTag(source: Source): CreateModelCommandData {
    return createClientPmHelper(source, false, false, true)
  }

  private fun createClientPmHelper(source: Source, isClientPm: Boolean, isClientProperty: Boolean, hasClientTag: Boolean): CreateModelCommandData {
    return PresentationModelBuilder(clientModelId, clientPmType, clientPm = isClientPm) {
      property(clientProperty, isClientProperty) {
        value = 13
        if (hasClientTag) clientTag(Tag.LABEL)
      }
    }
            .build(source)
  }
}
