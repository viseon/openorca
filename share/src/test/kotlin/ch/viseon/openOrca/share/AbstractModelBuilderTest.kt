/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

import ch.viseon.openOrca.share.SharedTestData.modelId
import ch.viseon.openOrca.share.SharedTestData.modelType
import ch.viseon.openOrca.share.SharedTestData.prop1
import ch.viseon.openOrca.share.SharedTestData.prop2
import kotlin.test.assertEquals

abstract class AbstractModelBuilderTest {

  open fun buildModelWithOneProperty() {
    val builder = PresentationModelBuilder(modelId, modelType) {
      property(prop1) {
        label = "Name"
      }
    }

    val data = builder.build(Source.REQUEST)
    val property = data.properties.first()
    assertEquals(prop1, property.name)
  }

  open fun buildModelWithSeveralPropertiesPreserveOrder() {
    val builder = PresentationModelBuilder(modelId, modelType) {
      property(prop2) {
        label = "Name"
      }
      property(prop1) {
        label = "Name"
      }
    }

    val pmData = builder.build(Source.REQUEST)
    val expectedOrder = ArrayList(listOf(prop2, prop1))
    val propertyNames = ArrayList<PropertyName>()
    propertyNames.addAll(pmData.properties.map { it.name })
    assertEquals(expectedOrder, propertyNames)
  }

}

