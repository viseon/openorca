/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue
import kotlin.test.fail

abstract class AbstractPropertyTest {

  open fun changeValue() {
    val prop1 = Property(SharedTestData.prop1, listOf<Pair<Tag,Any>>().asSequence())
    val value = "Label"
    prop1[Tag.VALUE] = value
    assertEquals(value, prop1.getValue())
  }

  open fun handlingPropertyValue() {
    val prop1 = Property(SharedTestData.prop1, listOf<Pair<Tag, Any>>().asSequence())

    var value: Any = "V1"
    prop1[Tag.VALUE] = value
    assertEquals(value, prop1.getValue())
    assertEquals(value, prop1.getValue())
    assertTrue(prop1.hasValue())

    value = SharedTestData.modelId
    prop1[Tag.VALUE] = value
    assertEquals(value, prop1.getValue<ModelId>())
    assertTrue(prop1.hasValue())
  }

  open fun setValueAndLabelValue() {
    //Setup
    val prop1 = Property(SharedTestData.prop1, listOf<Pair<Tag, Any>>().asSequence())
    val value = "A Value"
    val label = "A Label"
    val oldValue = prop1.set(Tag.VALUE,value)
    val oldLabel = prop1.set(Tag.LABEL, label)

    //Assert
    assertEquals(value, prop1.getValue<Any>())
    assertTrue(prop1.hasValue())
    assertNull(oldValue)

    assertEquals(label, prop1.getValue<Any>(Tag.LABEL))
    assertTrue(prop1.hasValue(Tag.LABEL))
    assertNull(oldLabel)
  }

  open fun getValues() {
    val prop1 = Property(SharedTestData.prop1, listOf<Pair<Tag, Any>>().asSequence())

    val value = "Label"
    prop1[Tag.LABEL] = value

    val values = prop1.getValues().toList()
    val actual = values.first()
    assertEquals(Tag.LABEL, actual.first)
    assertEquals(value, actual.second)
  }

  open fun setNotSupportedValue() {
    try {
      val prop1 = Property(SharedTestData.prop1, listOf<Pair<Tag, Any>>().asSequence())
      val value = listOf("A String")
      prop1[Tag.VALUE] = value
      fail("IllegalArgumentException expected")
    } catch(e: IllegalArgumentException) {
      //Test has passed
    }
  }
}