/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

import kotlin.test.assertEquals
import kotlin.test.assertTrue

abstract class AbstractJsonCodecTest {

  open fun decodeAndEncodeActionCommandData() {
    assertParsedCommand(SharedTestData.actionCommand())
  }

  open fun decodeAndEncodeRemoveModelCommandData() {
    assertParsedCommand(SharedTestData.removeModelCommand())
  }

  open fun decodeChangeValueCommandData() {
    assertParsedCommand(SharedTestData.changeValueCommand())
  }

  open fun decodeAndEncodeChangeValueCommandDataWithTagValue() {
    assertParsedCommand(ChangeValueCommandData(Source.REQUEST, SharedTestData.modelId, SharedTestData.prop1, Tag.VALUE, "Test", ApplyPolicy.DEFAULT))
  }

  open fun decodeAndEncodeChangeValueCommandWithIntegerValue() {
    assertParsedCommand(ChangeValueCommandData(Source.REQUEST, SharedTestData.modelId, SharedTestData.prop1, Tag.VALUE, 10, ApplyPolicy.DEFAULT))
  }

  open fun decodeCreateCommandData() {
    assertParsedCommand(SharedTestData.createModelCommand())
  }

  open fun decodeAndEncodeRemoveByTypeCommandData() {
    assertParsedCommand(SharedTestData.removeModelByTypeCommand())
  }

  open fun decodeAndEncodeSyncModelCommandData() {
    assertParsedCommand(SharedTestData.syncModelCommand())
  }

  open fun decodeAndEncodeAForcedCommand() {
    assertParsedCommand(SharedTestData.changeValueCommand(source = Source.REQUEST, policy = ApplyPolicy.FORCE_SEND))
  }

  open fun decodeAndEncodeCompositeCommand() {
    assertParsedCommand(SharedTestData.compositeCommand())
  }

  open fun decodeEncodeDoubleValue() {
    val value = 3.14
    decodeAndEncodeValue(value)
  }

  open fun decodeEncodeFloatValue() {
    val value = 3.14.toFloat()
    decodeAndEncodeValue(value)
  }

  open fun decodeEncodeIntValue() {
    val value = 3
    decodeAndEncodeValue(value)
  }

  open fun decodeEncodeStringValue() {
    val value = "ToolTip"
    decodeAndEncodeValue(value)
  }

  open fun decodeEncodeModelIdValue() {
    val value = SharedTestData.modelId2
    decodeAndEncodeValue(value)
  }

  open fun decodeAndEncodeBooleanValue() {
    val value = true
    decodeAndEncodeValue(value)
  }

  open fun decodeAndEncodeLongValue() {
    val value = 120L
    decodeAndEncodeValue(value)
  }

  open fun decodeAndEncodeModelType() {
    val value = SharedTestData.modelType
    decodeAndEncodeValue(value)
  }

  open fun encodeFaultyCommand() {
    val command = ChangeValueCommandData(
            Source.REQUEST,
            SharedTestData.modelId,
            SharedTestData.prop1,
            Tag.LABEL,
            listOf<String>() //Not a valid orca value
    )
    try {
      encodeAndDecodeCommand(command)
    } catch (e: ExceptionWithCause) {
      //  Exception should be decorated
      assertTrue(e.cause != null)
    }
  }

  private fun decodeAndEncodeValue(value: Any) {
    val json = valueToJson(createJsonObj(), Tag.VALUE, value)
    try {
      val (tag, parsedValue) = valueFromJson(json)
      assertEquals(value, parsedValue, "json: '${json.toJsonString()}'")
      assertEquals(Tag.VALUE, tag, "json: '${json.toJsonString()}'")
    } catch (e: Exception) {
      throw RuntimeException("Error parsing json: ${json.toJsonString()}")
    }
  }

  private fun encodeAndDecodeCommand(commandData: CommandData): Pair<CommandData, String> {
    val codec = JsonCodec(createJsonFactory())
    val json = codec.encode(listOf(commandData))
    val parsedCommands = codec.decode(Source.REQUEST, json)
    assertEquals(1, parsedCommands.count(), "Array has wrong size. Produced json: \n$json")
    return Pair(parsedCommands.first(), json)
  }

  private fun assertParsedCommand(commandData: CommandData) {
    val (command, json) = encodeAndDecodeCommand(commandData)
    assertEquals(commandData, command, "comparison failed. Produced json: \n$json")
  }

  private fun assertParsedCommand(commandData: CompositeCommandData) {
    val (command, json) = encodeAndDecodeCommand(commandData)
    val actualCompositeCommand = command as CompositeCommandData

    command.commands.forEachIndexed { index, expectedCommand ->
      val actualCommand = actualCompositeCommand.commands[index]
      if (expectedCommand is CompositeCommandData) {
        assertEquals(expectedCommand as CreateModelCommandData, actualCommand as CreateModelCommandData, json)
      } else {
        assertEquals(expectedCommand, actualCommand, "comparison failed. Produced json: \n$json")
      }
    }

  }

  private fun assertParsedCommand(commandData: CreateModelCommandData) {
    val (command, json) = encodeAndDecodeCommand(commandData)
    val actualCommand = command as CreateModelCommandData

    assertEquals(commandData, actualCommand, json)
  }

  private fun assertEquals(commandData: CreateModelCommandData, actualCommand: CreateModelCommandData, json: String) {
    assertEquals(commandData.modelId, actualCommand.modelId)
    assertEquals(commandData.modelType, actualCommand.modelType)
    assertEquals(commandData.clientOnly, actualCommand.clientOnly)

    val missingProperties = commandData.properties.filter { expectedProperty ->
      val actualProperty = actualCommand
              .properties
              .find { it.name == expectedProperty.name }

      actualProperty?.let {
        assertEquals(actualProperty.clientOnly, expectedProperty.clientOnly)
        assertEquals(actualProperty.clientTags, expectedProperty.clientTags)
        true
      } ?: false
    }
    assertEquals(missingProperties.size, commandData.properties.toList().size, "comparison failed. Produced json: \n$json")
  }

  protected abstract fun createJsonFactory(): JsonFactory

  private fun createJsonObj(): JsonObj {
    return createJsonFactory().newJson { }
  }

}
