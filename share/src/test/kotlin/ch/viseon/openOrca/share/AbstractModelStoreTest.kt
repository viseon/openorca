/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

abstract class AbstractModelStoreTest {

  open fun containsModel() {
    //Setup
    val modelStore = DefaultPresentationModelStore()

    //Action && Assertion
    assertFalse(modelStore.contains(SharedTestData.modelId))

    modelStore.addModel(SharedTestData.createModel(SharedTestData.createModelCommand()))

    assertTrue(modelStore.contains(SharedTestData.modelId))
  }

  open fun containsModelByType() {
     //Setup
    val modelStore = DefaultPresentationModelStore()

    //Action && Assertion
    assertFalse(modelStore.contains(SharedTestData.modelType))

    modelStore.addModel(SharedTestData.createModel(SharedTestData.createModelCommand()))

    assertTrue(modelStore.contains(SharedTestData.modelType))
  }

  open fun getModelById() {
    //Setup
    val modelStore = SharedTestData.createModelStoreWithOneModel()
    //Action
    modelStore.addModel(SharedTestData.createModel(SharedTestData.createModelCommand()))
    //Assertion
    val model = modelStore[SharedTestData.modelId]
    assertEquals(SharedTestData.modelId, model.id)
  }

  open fun getModelByTypeId() {
    //Setup
    val modelStore = SharedTestData.createModelStoreWithOneModel()
    //Action
    val byType = modelStore[SharedTestData.modelType].first()
    //Assertion
    assertEquals(SharedTestData.modelId, byType.id)
    assertEquals(SharedTestData.modelType, byType.type)
  }

  open fun removeModelByType() {
    //Setup
     val modelStore = SharedTestData.createModelStoreWithTwoModels()

    //Action
    val removedModels = modelStore.removeModels(SharedTestData.modelType)

    //Assertions
    val removedModel = removedModels.first()
    assertEquals(SharedTestData.modelId, removedModel.id)
    assertEquals(SharedTestData.modelType, removedModel.type)

    val remainingModels = modelStore[SharedTestData.modelType2]
    assertEquals(1, remainingModels.size)
    val remainingModel = remainingModels.first()
    assertEquals(SharedTestData.modelId2, remainingModel.id)
    assertEquals(SharedTestData.modelType2, remainingModel.type)
  }

  open fun removeModel() {
    //Setup
    val modelStore = SharedTestData.createModelStoreWithOneModel()

    //Action
    val removedPm = modelStore.removeModel(SharedTestData.modelId)

    //Assertion
    assertEquals(SharedTestData.modelId, removedPm.id)
    assertFalse(modelStore.contains(SharedTestData.modelId))
  }

  open fun containsByType() {
     //Setup
    val modelStore = SharedTestData.createModelStoreWithOneModel()

    //Action
    val contains = modelStore.contains(SharedTestData.modelType)

    //Assertion
    assertTrue(contains)
  }

  open fun getAllModels() {
    //setup
    val modelStore= SharedTestData.createModelStoreWithOneModel()

    //Action
    val models = modelStore.getAllModels()

    //Assertion
    assertEquals(1, models.size, "getAllModels() did not return the correct number of models")
    assertEquals(SharedTestData.modelId, models.first().id)
  }

  open fun getAllModelsHonorsInsertionOrder() {

    val modelStore = DefaultPresentationModelStore()

    //setup
    modelStore.addModel(SharedTestData.createModel(SharedTestData.createPm2WithOneProperty()))
    modelStore.addModel(SharedTestData.createModel(SharedTestData.createPmWithOneProperty()))

    //Action
    val models = modelStore.getAllModels()

    //Assertion
    assertEquals(2, models.size, "getAllModels() did not return the correct number of models")
    assertEquals(listOf(SharedTestData.modelId2, SharedTestData.modelId), models.map { it.id })
  }


}

