/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share.command

import ch.viseon.openOrca.share.CommandData
import ch.viseon.openOrca.share.Event

class CommandApplication private constructor(val applied: Boolean,
                                             private val shouldSendCommand: Boolean, val events: Iterable<Event>, val commandData: CommandData) {

  fun sendCommand(): Boolean {
    return shouldSendCommand
  }

  companion object {

    fun clientCommandApplied(events: Iterable<Event>, commandData: CommandData): CommandApplication {
      return CommandApplication(true, false, events, commandData)
    }

    /**
     * Command was applied
     */
    fun applied(events: Iterable<Event>, commandData: CommandData): CommandApplication {
      return CommandApplication(true, true, events, commandData)
    }

    /**
     *  Command was rejected
     */
    fun rejected(commandData: CommandData): CommandApplication {
      return CommandApplication(false, commandData.policy.isForceSend(), listOf(), commandData)
    }

    /**
     * Only send the command to the client.
     */
    fun clientOnlyCommand(commandData: CommandData): CommandApplication {
      return CommandApplication(false, true, listOf(), commandData)
    }
  }
}