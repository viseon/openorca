/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

class PresentationModelBuilder(
        val modelId: ModelId,
        val modelType: ModelType,
        val clientPm: Boolean = false,
        block: PresentationModelBuilder.() -> Unit) : ModelBuilder {


  private val properties: MutableList<Property> = ArrayList()

  val propertyList: List<Property> get() {
    return properties
  }

  init {
    block()
  }

  override fun build(source: Source, policy: ApplyPolicy): CreateModelCommandData {
    return CreateModelCommandData(source, modelId, modelType, properties, clientPm, policy)
  }

  fun property(name: PropertyName, clientProperty: Boolean = false, block: PropertyBuilder.() -> Unit) {
    val builder = PropertyBuilder(name, clientProperty)
    builder.block()
    properties.add(builder.build())
  }

  inner class PropertyBuilder(val name: PropertyName, val clientProperty: Boolean = false) {

    val values: MutableMap<Tag, Any> = LinkedHashMap()
    val clientTags = mutableSetOf<Tag>()

    var label: String
      set(value) {
        set(Tag.LABEL, value)
      }
      get() {
        return this[Tag.LABEL] as String
      }

    var toolTip: String
      set(value) {
        set(Tag.TOOL_TIP, value)
      }
      get() {
        return this[Tag.TOOL_TIP] as String
      }


    var value: Any
      set(value) {
        set(Tag.VALUE, value)
      }
      get() {
        return this[Tag.VALUE]
      }


    fun set(tag: Tag, propertyValue: Any) {
      values[tag] = propertyValue
    }

    operator fun get(typeName: Tag): Any {
      return values[typeName]!!
    }

    fun clientTag(tag: Tag) {
      clientTags.add(tag)
    }

    fun build(): Property {
      return  Property(name, values.entries.map { (key, value) -> Pair(key, value) }.asSequence(), clientPm || clientProperty, clientTags)
    }

  }

}
