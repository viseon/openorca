/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

interface PresentationModel {

  val id: ModelId
  val type: ModelType
  /**
   * All data of this presentation model only exist on the client side. E.g. If the controller has to send
   * a list of data, for which we only care about the ID of the PM and not the actual data. (On the controller side)
   */
  val clientOnly: Boolean

  fun getPropertiesArray(): Array<Property>

  fun getProperties(): Sequence<Property>

  //@JsName("getByType")
  operator fun get(propertyName: PropertyName): Property

  fun propertyCount(): Int

  fun hasProperty(name: PropertyName): Boolean

}

