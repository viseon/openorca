/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

enum class ApplyPolicy {

  /**
   * Only process (send or apply) a command, if the state is different from the new one. If a ChangeValueCommand
   * should set a value to a given property, it will only do so, if the property has a different value.
   * E.g. If Property "age" has Value "33", it will not apply a ChangeValueCommand which will again set value "33"
   */
  DEFAULT,

  /**
   * Forces this side to send this command to the other side. The command will be forcefully applied on the other side.
   * This Policy is used to resend all pms to the client. E.g. The Web-Browser refreshes its page.
   */
  FORCE_SEND,

  /**
   * Forces the application of this command and forcefully sends it to the other side.
   */
  FORCE_APPLY;

  fun isForceSend(): Boolean {
    return this == FORCE_SEND || isForceApply()
  }

  fun isForceApply(): Boolean {
    return this == FORCE_APPLY
  }

  fun isDefault(): Boolean {
    return this == DEFAULT
  }
}

/**
 * Command data serves as a carrier of a change: Create a new model, or change a value from a particular model.
 *
 * Clients can implement there on commands. Check serialization of commands from a Request and deserialization to a Response.
 */
interface CommandData {

  val source: Source

  val policy: ApplyPolicy
}

/**
 * @param source specifies the source of the change
 * @param modelId specifies the model
 * @param propertyName specifies the property - must exist in the given model otherwise an exception will be thrown.
 * @param tag specifies the type of the data that should change (Tag.LABEL, Tag.FOCUS etc.)
 * @param policy Specifies the ApplyPolicy.
 */
data class ChangeValueCommandData(override val source: Source,
                                  val modelId: ModelId,
                                  val propertyName: PropertyName,
                                  val tag: Tag,
                                  val value: Any,
                                  override val policy: ApplyPolicy = ApplyPolicy.DEFAULT
                                  ) : CommandData

/**
 *
 * @param source specifies the source of the change
 * @param modelId specifies the model
 * @param modelType ModelType of the model
 * @param clientOnly Is this a client only PM? If so, only the modelId.
 * @param policy Specifies the ApplyPolicy.
 */
data class CreateModelCommandData(override val source: Source,
                                  val modelId: ModelId,
                                  val modelType: ModelType,
                                  val properties: Collection<Property>,
                                  val clientOnly: Boolean = false,
                                  override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData

/**
 *
 * @param source specifies the source of the change
 * @param modelId specifies the model
 * @param modelType ModelType of the model
 * @param policy Specifies the ApplyPolicy.
 */
data class RemoveModelCommandData(override val source: Source,
                                  val modelId: ModelId,
                                  val modelType: ModelType,
                                  override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData

/**
 *
 * @param source specifies the source of the change
 * @param actionName Arbitrary actions that should be executed. Can be observed registerNamedCommand(actionName: String): Observable<ActionEvent>
 * @param modelIds models that server as data for the command.
 * @param policy Specifies the ApplyPolicy.
 */
data class ActionCommandData(override val source: Source,
                             val actionName: String,
                             val modelIds: List<ModelId> = listOf(),
                             override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData

/**
 * Executed n commands as one.
 *
 * @param source specifies the source of the change
 * @param commands commands that should be executed as one.
 */
data class CompositeCommandData(override val source: Source,
                                val commands: List<CommandData>): CommandData {

  override val policy = ApplyPolicy.DEFAULT

}


/**
 * Removes all models of a certain type.
 */
data class RemoveModelByTypeCommandData(
    override val source: Source,
    val modelType: ModelType,
    override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData

/**
 * Copy one model onto another.
 */
data class SyncModelCommandData(
    override val source: Source,
    val sourceModel: ModelId,
    val destinationModel: ModelId,
    override val policy: ApplyPolicy = ApplyPolicy.DEFAULT) : CommandData

