/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

import ch.viseon.openOrca.share.command.CommandApplication
import ch.viseon.openOrca.share.command.execution.*


object CommandApplier {

  private val executors = mapOf(
          CreateModelCommandData::class.simpleName!! to CreateModelExecutor,
          RemoveModelCommandData::class.simpleName!! to RemoveModelExecutor,
          ActionCommandData::class.simpleName!! to ActionExecutor,
          ChangeValueCommandData::class.simpleName!! to ChangeValueExecutor,
          RemoveModelByTypeCommandData::class.simpleName!! to RemoveModelByTypeExecutor,
          SyncModelCommandData::class.simpleName!! to SyncModelExecutor,
          CompositeCommandData::class.simpleName!! to CompositeCommandExecutor

  )

  fun apply(source: Source, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    return apply(source, false, true, modelStore, commandData)
  }

  fun apply(source: Source, isCompositeUpdate: Boolean, isLastEventOfCompositeUpdate: Boolean, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    val executor = executors[commandData::class.simpleName!!]
            ?: throw IllegalArgumentException("No executor found for commandData: '$commandData'")
    try {
      return executor.execute(
              source,
              isCompositeUpdate,
              isLastEventOfCompositeUpdate,
              modelStore,
              commandData
      )
    } catch (e: ExceptionWithCause) {
      //Only Throwable can have a cause (error in kotlin std-lib)
      throw ExceptionWithCause("Failed to apply command: '$commandData'. Original error message: '${e.message}'", e)
    }
  }

}

