/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share.command.execution

import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.command.CommandApplication

object RemoveModelByTypeExecutor : CommandExecutor {

  override fun execute(source: Source, isCompositeUpdate: Boolean, isLastEvetOfCompositeUpdate: Boolean, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "Execute RemoveModelByType. Data: '$commandData" }
    val it = commandData as RemoveModelByTypeCommandData

    if (!forceApply(source, it) && it.modelType !in modelStore) {
      return CommandApplication.rejected(commandData)
    }

    val models = modelStore.removeModels(it.modelType)

    fun isLastEvent(index: Int): Boolean {
      return (isCompositeUpdate && isLastEvetOfCompositeUpdate && (index+1 == models.size))
      || (!isCompositeUpdate && (index+1== models.size))
    }

    return CommandApplication.applied(
            models
                    .mapIndexed { index, pm ->
                      ModelStoreChangeEvent(
                              it.source,
                              true,
                              isLastEvent(index),
                              pm.id,
                              pm.type,
                              ModelStoreChangeEventType.REMOVE
                      )
                    },
            commandData
    )
  }

}