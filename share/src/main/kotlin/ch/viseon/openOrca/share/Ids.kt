/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

data class ModelId(val stringId: String) {

  override fun toString(): String {
    return stringId
  }

}

data class ModelType(val stringId: String) {

  override fun toString(): String {
    return stringId
  }


}

data class PropertyName(val name: String) {
  @Deprecated("use value directly", ReplaceWith("name"))
  fun asValue(): String {
    return name
  }

  override fun toString(): String {
    return name
  }

}



