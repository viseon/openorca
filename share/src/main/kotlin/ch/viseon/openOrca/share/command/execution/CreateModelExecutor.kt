/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share.command.execution

import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.command.CommandApplication
import ch.viseon.openOrca.share.impl.DefaultPresentationModel

object CreateModelExecutor : CommandExecutor {

  override fun execute(source: Source, isCompositeUpdate: Boolean, isLastEvetOfCompositeUpdate: Boolean, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "applyingCommand data: $commandData" }

    val it = commandData as CreateModelCommandData

    //Client Only PM
    if (commandData.clientOnly && source.isResponse()) {
      val model = DefaultPresentationModel(it.modelId, it.modelType, commandData.clientOnly, emptySequence())
      modelStore.addModel(model)
      return CommandApplication.clientOnlyCommand(commandData)
    }

    if (!forceApply(source, it) && modelStore.contains(commandData.modelId)) {
      return CommandApplication.rejected(commandData)
    }

    val properties = it.properties
            .asSequence()
            .filter { !(it.clientOnly && source.isResponse()) }

    val model = DefaultPresentationModel(it.modelId, it.modelType, commandData.clientOnly, properties)
    modelStore.addModel(model)
    val events = listOf(ModelStoreChangeEvent(
            it.source,
            isCompositeUpdate,
            isLastEvetOfCompositeUpdate,
            model.id,
            model.type,
            ModelStoreChangeEventType.ADD
    ))

    if (model.clientOnly) {
      return CommandApplication.clientCommandApplied(events, commandData)
    }

    return CommandApplication.applied(events, commandData
    )
  }
}