/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share.command.execution

import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.command.CommandApplication

object CompositeCommandExecutor : CommandExecutor {

  override fun execute(source: Source, isCompositeUpdate: Boolean, isLastEvetOfCompositeUpdate: Boolean, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    val data = commandData as CompositeCommandData
    val commandCount = data.commands.size
    val events = data.commands
            .asSequence()
            .mapIndexed { index, command -> CommandApplier.apply(source, true, (commandCount == (index+1)), modelStore, command) }
            .filter { it.applied }
            .flatMap { it.events.asSequence() }
            .toList()

    return CommandApplication.applied(events, data)
  }

}