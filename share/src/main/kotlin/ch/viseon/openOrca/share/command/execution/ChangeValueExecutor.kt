/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share.command.execution

import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.command.CommandApplication

object ChangeValueExecutor : CommandExecutor {

  override fun execute(source: Source, isCompositeUpdate: Boolean, isLastEvetOfCompositeUpdate: Boolean, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    val it = commandData as ChangeValueCommandData

    fun valueAlreadyPresent(): Boolean {
      return modelStore[commandData.modelId][commandData.propertyName].let { it.hasValue(commandData.tag) && it[commandData.tag] == commandData.value }
    }

    val model = modelStore[it.modelId]
    if (model.clientOnly && source.isResponse()) {
      return CommandApplication.clientOnlyCommand(commandData)
    }


    if (!forceApply(source, it) && valueAlreadyPresent()) {
//      LOGGER.finer { "ChangeValueCommand rejected. Data: '$commandData" }
      return CommandApplication.rejected(commandData)
    }

    val property = model[it.propertyName]
    if ((property.isClientTag(it.tag)) && source.isResponse()) {
      return CommandApplication.clientOnlyCommand(commandData)
    }

    val clientOnlyChange = model.clientOnly || property.clientOnly || property.isClientTag(it.tag)

//      LOGGER.finer { "ChangeValueCommand applied. Data: '$commandData" }
    val oldValue = property.set(it.tag, it.value)
    val events = listOf(PropertyChangeEvent(
            it.source,
            isCompositeUpdate,
            isLastEvetOfCompositeUpdate,
            model.id,
            model.type,
            ValueChangeEvent(
                    it.source,
                    isCompositeUpdate,
                    isLastEvetOfCompositeUpdate,
                    it.propertyName,
                    it.tag,
                    oldValue,
                    it.value
            )
    ))

    return if (clientOnlyChange) {
      CommandApplication.clientCommandApplied(events, commandData)
    } else {
      CommandApplication.applied(
              events,
              commandData
      )
    }
  }

}