/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share.command.execution

import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.command.CommandApplication

object SyncModelExecutor : CommandExecutor {

  override fun execute(source: Source, isCompositeUpdate: Boolean, isLastEvetOfCompositeUpdate: Boolean, modelStore: ModelStore, commandData: CommandData): CommandApplication {
    //LOGGER.finer { "Execute SyncModel. Data: '$commandData" }

    val data = commandData as SyncModelCommandData
    val sourcePm = modelStore[data.sourceModel]
    val destinationPm = modelStore[data.destinationModel]
    val events = sourcePm.getProperties()
            .filter { property -> destinationPm.hasProperty(property.name) }
            .flatMap { property: Property ->
              property.getValues()
                      .map { pair: Pair<Tag, Any> ->
                        val (tag, value) = pair
                        //Values are immutable
                        val oldValue = destinationPm[property.name].set(tag, value)
                        PropertyChangeEvent(
                                data.source,
                                isCompositeUpdate,
                                isLastEvetOfCompositeUpdate,
                                destinationPm.id,
                                destinationPm.type,
                                ValueChangeEvent(
                                        data.source,
                                        isCompositeUpdate,
                                        isLastEvetOfCompositeUpdate,
                                        property.name,
                                        tag,
                                        oldValue,
                                        value
                                )
                        )
                      }
            }
            .toList()
    return CommandApplication.applied(events, commandData)
  }

}