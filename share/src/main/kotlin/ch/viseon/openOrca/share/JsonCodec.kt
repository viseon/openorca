/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.share

class JsonArray<T>(val values: Iterable<T>) : Iterable<T> by values

interface JsonObj {

  operator fun set(key: String, value: String)
  operator fun set(key: String, value: Array<String>)
  operator fun set(key: String, value: Boolean)
  operator fun set(key: String, block: JsonObj.() -> Unit)
  operator fun set(key: String, value: JsonObj)
  operator fun set(key: String, value: JsonArray<JsonObj>)
  operator fun set(key: String, value: Iterable<String>)

  fun string(key: String): String
  fun optionalString(key: String): String?

  fun boolean(key: String): Boolean
  fun optionalBoolean(key: String): Boolean?

  fun getObject(key: String): JsonObj

  /**
   * Primitive array
   */
  fun <T> pArray(key: String): JsonArray<T>

  /**
   * Object-Array
   */
  fun oArray(key: String): JsonArray<JsonObj>

  fun toJsonString(): String
  fun integer(key: String): Int
  fun long(key: String): Long
  fun float(key: String): Float
  fun double(key: String): Double

}

interface JsonFactory {
  fun parse(content: String): JsonArray<JsonObj>

  fun newJson(block: JsonObj.() -> Unit): JsonObj

}

class JsonCodec(private val jsonFactory: JsonFactory) : StringCodec {

  override val mimeType = "application/json"
  override val encoding = "UTF-8"

  private val commandCodecs = mapOf(
          ActionCommandData::class.simpleName!! to ActionCommandCodec(jsonFactory),
          RemoveModelCommandData::class.simpleName!! to RemoveModelCommandCodec(jsonFactory),
          RemoveModelByTypeCommandData::class.simpleName!! to RemoveModelByTypeCodec(jsonFactory),
          ChangeValueCommandData::class.simpleName!! to ChangeValueCommandCodec(jsonFactory),
          CreateModelCommandData::class.simpleName!! to CreateModelCommandCodec(jsonFactory),
          SyncModelCommandData::class.simpleName!! to SyncModelCommandCodec(jsonFactory),
          CompositeCommandData::class.simpleName!! to CompositeCommandCodec(jsonFactory, this)
  )

  override fun decode(source: Source, data: String): Iterable<CommandData> {
    if (data.isEmpty()) {
      return emptyList()
    }

    try {
      val json = jsonFactory.parse(data)
      return json.map {
        decodeCommandData(source, it)
      }
    } catch (e: ExceptionWithCause) {
      throw ExceptionWithCause("Could not parse json $data", e)
    }
  }

  fun decodeCommandData(source: Source, it: JsonObj): CommandData {
    val type = it.string("type")
    return commandCodecs[type]!!.decode(source, it)
  }

  fun encodeToJsonObj(commandData: CommandData): JsonObj {
    return commandCodecs[commandData::class.simpleName!!]
            ?.let { codec ->
              try {
                codec.encode(commandData)
              } catch (e: ExceptionWithCause) {
                throw ExceptionWithCause("Error processing command $commandData", e)
              }
            }
            ?: throw IllegalArgumentException("No Codec found for command $commandData")
  }

  override fun encode(commands: Iterable<CommandData>): String {


    return buildString {
      append("[")
      append(
              commands
                      .asSequence()
                      .map { encodeToJsonObj(it) }
                      .map { it.toJsonString() }
                      .joinToString(",")
      )
      append("]")
    }
  }

}


interface CommandCodec {
  fun encode(untypedCommandData: CommandData): JsonObj
  fun decode(source: Source, json: JsonObj): CommandData
}

private class ActionCommandCodec(val jsonFactory: JsonFactory) : CommandCodec {

  override fun encode(untypedCommandData: CommandData): JsonObj {
    val typedCommand = untypedCommandData as ActionCommandData

    val jsonObj = jsonFactory.newJson {
      this["type"] = ActionCommandData::class.simpleName!!
      this["data"] = {
        if (!typedCommand.policy.isDefault()) this["policy"] = typedCommand.policy.name
        this["actionName"] = typedCommand.actionName
        this["modelIds"] = typedCommand.modelIds.map { it.stringId }
      }
    }

    return jsonObj
  }

  override fun decode(source: Source, json: JsonObj): CommandData {
    val data = json.getObject("data")
    val policy = data.optionalString("policy")?.let { ApplyPolicy.valueOf(it) } ?: ApplyPolicy.DEFAULT
    val modelIds: JsonArray<String> = data.pArray("modelIds")
    return ActionCommandData(source, data.string("actionName"), modelIds.map(::ModelId), policy)
  }

}

private class RemoveModelCommandCodec(private val jsonFactory: JsonFactory) : CommandCodec {

  override fun encode(untypedCommandData: CommandData): JsonObj {
    val typedCommand = untypedCommandData as RemoveModelCommandData

    return jsonFactory.newJson {
      this["type"] = RemoveModelCommandData::class.simpleName!!
      this["data"] = {
        this["modelId"] = typedCommand.modelId.stringId
        this["modelType"] = typedCommand.modelType.stringId
      }
    }
  }

  override fun decode(source: Source, json: JsonObj): CommandData {
    val data = json.getObject("data")
    return RemoveModelCommandData(source, ModelId(data.string("modelId")), ModelType(data.string("modelType")))
  }

}

private class ChangeValueCommandCodec(private val jsonFactory: JsonFactory) : CommandCodec {

  override fun encode(untypedCommandData: CommandData): JsonObj {
    val changeValueCommand = untypedCommandData as ChangeValueCommandData

    return jsonFactory.newJson {
      this["type"] = ChangeValueCommandData::class.simpleName!!
      this["data"] = {
        if (!changeValueCommand.policy.isDefault()) this["policy"] = changeValueCommand.policy.name
        this["modelId"] = changeValueCommand.modelId.stringId
        this["propertyName"] = changeValueCommand.propertyName.name
        this["value"] = jsonFactory.newJson { valueToJson(this, changeValueCommand.tag, changeValueCommand.value) }
      }
    }
  }

  override fun decode(source: Source, json: JsonObj): CommandData {
    val data: JsonObj = json.getObject("data")
    val policy = data.optionalString("policy")?.let { ApplyPolicy.valueOf(it) } ?: ApplyPolicy.DEFAULT
    val modelId = ModelId(data.string("modelId"))
    val propertyName = PropertyName(data.string("propertyName"))
    val (tag, value) = valueFromJson(data.getObject("value"))
    return ChangeValueCommandData(source, modelId, propertyName, tag, value, policy)
  }

}

private class CreateModelCommandCodec(private val jsonFactory: JsonFactory) : CommandCodec {

  override fun encode(untypedCommandData: CommandData): JsonObj {
    val createCommand = untypedCommandData as CreateModelCommandData

    return jsonFactory.newJson {
      this["type"] = CreateModelCommandData::class.simpleName!!
      this["data"] = {
        this["modelId"] = createCommand.modelId.stringId
        this["type"] = createCommand.modelType.stringId
        createCommand.clientOnly.takeIf { it }?.also { this["clientOnly"] = true }
        this["properties"] = JsonArray(
                createCommand.properties
                        .map {
                          jsonFactory.newJson {
                            this["propertyName"] = it.name.name
                            it.clientOnly.takeIf { it }?.also { this["clientOnly"] = true }
                            this["clientTags"] = it.clientTags.map { it.name }
                            this["values"] = JsonArray(it.getValues()
                                    .map { (tag, value) -> jsonFactory.newJson { valueToJson(this, tag, value) } }
                                    .toList())
                          }
                        })
      }
    }
  }

  override fun decode(source: Source, json: JsonObj): CommandData {
    val data = json.getObject("data")
    val clientPm = data.optionalBoolean("clientOnly") ?: false
    val properties: JsonArray<JsonObj> = data.oArray("properties")
    return CreateModelCommandData(
            source,
            ModelId(data.string("modelId")),
            ModelType(data.string("type")),
            properties.map { propertyJson ->
              val clientProperty = propertyJson.optionalBoolean("clientOnly") ?: false
              val values: JsonArray<JsonObj> = propertyJson.oArray("values")
              Property(
                      PropertyName(propertyJson.string("propertyName")),
                      values.asSequence().map(::valueFromJson),
                      clientProperty,
                      propertyJson.pArray<String>("clientTags").asSequence().map { Tag(it) }.toSet()
              )
            },
            clientPm)
  }

}

class RemoveModelByTypeCodec(private val jsonFactory: JsonFactory) : CommandCodec {

  override fun encode(untypedCommandData: CommandData): JsonObj {
    val commandData = untypedCommandData as RemoveModelByTypeCommandData

    return jsonFactory.newJson {
      this["type"] = RemoveModelByTypeCommandData::class.simpleName!!
      this["data"] = {
        this["modelTypeId"] = commandData.modelType.stringId
      }
    }
  }

  override fun decode(source: Source, json: JsonObj): CommandData {
    val data = json.getObject("data")
    return RemoveModelByTypeCommandData(source, ModelType(data.string("modelTypeId")))
  }

}

class SyncModelCommandCodec(private val jsonFactory: JsonFactory) : CommandCodec {

  override fun encode(untypedCommandData: CommandData): JsonObj {
    val commandData = untypedCommandData as SyncModelCommandData

    return jsonFactory.newJson {
      this["type"] = SyncModelCommandData::class.simpleName!!
      this["data"] = {
        this["src"] = commandData.sourceModel.stringId
        this["dst"] = commandData.destinationModel.stringId
      }
    }
  }

  override fun decode(source: Source, json: JsonObj): CommandData {
    val data = json.getObject("data")
    return SyncModelCommandData(source, ModelId(data.string("src")), ModelId(data.string("dst")))
  }

}

class CompositeCommandCodec(private val jsonFactory: JsonFactory, private val jsonCodec: JsonCodec) : CommandCodec {

  override fun encode(untypedCommandData: CommandData): JsonObj {
    val commandData = untypedCommandData as CompositeCommandData

    return jsonFactory.newJson {
      this["type"] = CompositeCommandData::class.simpleName!!
      this["data"] = JsonArray(commandData.commands.map { jsonCodec.encodeToJsonObj(it) })
    }
  }

  override fun decode(source: Source, json: JsonObj): CommandData {
    val data = json.oArray("data")
    return CompositeCommandData(source,
            data.map {
              jsonCodec.decodeCommandData(source, it)
            }
                    .toList()
    )
  }

}

fun valueToJson(jsonObj: JsonObj, tag: Tag, value: Any): JsonObj {
  with(jsonObj) {
    if (value !is String) {
      this["type"] = value.getJsonTypeId()
    }
    this["value"] = value.asJsonValue()
    if (tag != Tag.VALUE) {
      this["tag"] = tag.name
    }
  }
  return jsonObj
}

private fun Any.getJsonTypeId(): String {
  return when (this) {
    is String -> "String"
    is Boolean -> "Boolean"
    is Double -> "Double"
    is Float -> "Float"
    is Long -> "Long"
    is Int -> "Int"
    is Char -> "Char"
    is Short -> "Short"
    is ModelId -> "ModelId"
    is ModelType -> "ModelType"
    else -> throw IllegalArgumentException("Type of value '$this' (class.simpleName: '${this::class.simpleName}') not be supported.")
  }
}

fun isSupported(value: Any): Boolean {
  return value is String
          || value is Boolean
          || value is Double
          || value is Float
          || value is Long
          || value is Int
          || value is Char
          || value is Short
          || value is ModelId
          || value is ModelType
}

private fun Any.asJsonValue(): String {
  return toString()
}

fun valueFromJson(json: JsonObj): Pair<Tag, Any> {
  val type = json.optionalString("type") ?: "String"
  val value: Any = when (type) {
    "String" -> json.string("value")
    "Boolean" -> json.boolean("value")
    "Int" -> json.integer("value")
    "Long" -> json.long("value")
    "Float" -> json.float("value")
    "Double" -> json.double("value")
    "Char" -> json.string("value")[0]
    "Short" -> json.integer("value").toShort()
    "ModelId" -> ModelId(json.string("value"))
    "ModelType" -> ModelType(json.string("value"))
    else -> {
      throw IllegalArgumentException("Unknown propertyValue type: '$type'")
    }
  }

  val tag = json.optionalString("tag") ?: Tag.VALUE.name
  return Pair(Tag(tag), value)
}
