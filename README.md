#What it is
OpenOrca is written in Kotlin and is a Presentation Model Pattern implementation. (https://martinfowler.com/eaaDev/PresentationModel.html) 
Instead of specific PMs there are general PMs that can be build with a `PresentationModelBuilder`.
Example: 

    PresentationModelBuilder( modelId, modelType) {
      property(prop1) {
        label = StringValue(prop1.name)
      }
    }
    
(Source: `SharedTestData.createModelWithOneProperty()`)
    
##Motivation
The goal is to strictly separate UI and Controller logic. 
Ideally you only have a thin layer of binding logic to a particular UI technology (we used cycle.js)
In our fast paced Javascript world this approach allows you to exchange the given UI technology within a reasonable time frame.

Currently the controller logic is sitting on the server side (JVM) and sends back PMs to the client.
That means that models are living on the client side and on the server side. (Sessions are used to store PMs server side)

In the (near) future, it would be nice to deploy the controller to the client, 
so the server session is not bloated with PMs. 
For this we would need the possibility to build modules with interfaces
implemented by a specific platform. (Breslav explained this idea in a presentation)

#How it works
There is an example project here: https://bitbucket.org/viseon/example (example project is not very mature yet)

OpenOrca uses Rx as part of the API design:

Two methods of `OrcaSource` main interface for implementing controller logic:

````kotlin
fun observeModelStore(): Observable<ModelStoreChangeEvent>
fun observeModel(modelId: ModelId): Observable<PropertyChangeEvent>
````

This allows you to write following code:

````kotlin
fun handler(source: OrcaSource): Observable<CommandData> {
  val `createModel$` = source
      .observeModelStore()
      .filter { it.modelId == SharedTestData.modelId }
      .flatMap { Observable.just(changeValueCommand) }

  val `valueChanged$` = source
      .observeProperty(SharedTestData.modelId, SharedTestData.prop1)
      .filter { it.isValueChange() }
      .flatMap { Observable.just(changeLabelCommand) }
}
````
    
(Source: `OrcaRunTest.process multiple Event with back feed`)

##Explanation of Command -> Event -> Command
On the client side, orca receives a command via `ClientOrca.executeCommands()`. The commands will be executed and from this execution
events will be generated. You can say a command maps to n-events. Depending of the execution result (command was applied or not) the command
will be send to the controller as part of a request (via JSON - default codec if in wire mode). The controller
will execute the received commands and dispatch all events. The event notification happens during command application. 
That means not all commands may be applied when a particular event is processed. The event processing will lead to new commands
which will be send back to the server. Before these commands are send back to the server, the are also applied 
This results in a cycle during the request processing: Every command application leads to an event, which will lead to a command which can lead to an event. Its the application programmers responsibility to prevent an endless loop.
When no more commands are generated, the response is complete and send back to the client. The client will then process the events from the initial
commands and also the commands from the server. At the end of the request, client and server have the same state again.

###ForceSend and double application
In general commands are not applied twice. Lets say the client changes the same value twice, the second command will not be applied
and not send to the controller. You can forcibly send a command to the other side with `forceSend=true`. The other side
will then also apply the command.

#Setup
The java part of the library is in maven central, 

    //Gradle
    compile group: 'ch.viseon.openOrca.server', name: 'core', version: '0.0.18'
    compile group: 'ch.viseon.openOrca.server', name: 'server-servlet', version: '0.0.18'

The JS part can be obtained via NPM (https://www.npmjs.com/package/openorca-js-client)
          
JS (in TypeScript) Setup:

````ts
function createClientOrca(): ClientOrcaImpl {
    let commandListExecutor = new ClientCommandListExecutor();
    let serverUrl = "http://localhost:8080/orca";
    let codec = new JsonCodec(new JsonFactoryJs());
    let transmitter = new XMLHttpRequestTransmitter(
        serverUrl,
        codec
    );
    return new ClientOrcaImpl(commandListExecutor, transmitter);
}
````

JVM (controller) setup:

````kotlin
class OrcaServlet : DefaultOrcaServlet(Configuration("http://localhost:63342")) {

    override fun processRequest(orcaSource: OrcaSource): Observable<CommandData> {
       // Your code here (see example above)
    }
}
````
