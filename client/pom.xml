<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright 2017 viseon gmbh
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>ch.viseon</groupId>
        <artifactId>openOrca</artifactId>
        <version>1.0.1</version>
    </parent>

    <groupId>ch.viseon.openOrca</groupId>
    <artifactId>openorca-js-client</artifactId>

    <properties>
        <maven.test.skip>false</maven.test.skip>
        <main.file.name>index.js</main.file.name>
        <main.meta.file.name>index.meta.js</main.meta.file.name>
        <main.file.path>${project.basedir}/dist/${main.file.name}</main.file.path>
        <main.meta.file.path>${project.basedir}/dist/${main.meta.file.name}</main.meta.file.path>
    </properties>

    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>bintray-danfma-maven</id>
            <name>bintray</name>
            <url>http://dl.bintray.com/danfma/maven</url>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib-js</artifactId>
            <version>${kotlin.version}</version>
        </dependency>
        <dependency>
            <groupId>ch.viseon.openOrca</groupId>
            <artifactId>share</artifactId>
            <version>1.0.1</version>
        </dependency>
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-test-js</artifactId>
            <version>${kotlin.version}</version>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>br.danfma</groupId>
            <artifactId>kodando-runtime</artifactId>
            <version>0.4.1</version>
        </dependency>
        <dependency>
            <groupId>br.danfma</groupId>
            <artifactId>kodando-rxjs</artifactId>
            <version>5.3.1-v2.1</version>
        </dependency>
    </dependencies>

    <profiles>
        <profile>
            <id>npm-install-for-karma-tests</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>

            <build>
                <plugins>
                    <plugin>
                        <groupId>com.github.eirslett</groupId>
                        <artifactId>frontend-maven-plugin</artifactId>
                        <version>1.3</version>
                        <executions>
                            <execution>
                                <id>install node and npm</id>
                                <phase>generate-test-resources</phase>
                                <goals>
                                    <goal>install-node-and-npm</goal>
                                </goals>
                                <configuration>
                                    <skip>${maven.test.skip}</skip>
                                </configuration>
                            </execution>

                            <execution>
                                <id>npm install</id>
                                <phase>generate-test-resources</phase>
                                <goals>
                                    <goal>npm</goal>
                                </goals>
                                <!-- Optional configuration which provides for running any npm command -->
                                <configuration>
                                    <arguments>install</arguments>
                                    <skip>${maven.test.skip}</skip>
                                </configuration>
                            </execution>
                        </executions>
                        <configuration>
                            <nodeVersion>v0.12.17</nodeVersion>
                            <npmVersion>3.10.9</npmVersion>
                            <workingDirectory>${project.basedir}/src/test/karma</workingDirectory>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources-filtered</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-plugin</artifactId>
                <version>${kotlin.version}</version>
                <executions>
                    <execution>
                        <id>compile</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>js</goal>
                        </goals>
                        <configuration>
                            <sourceMap>true</sourceMap>
                            <moduleKind>umd</moduleKind>
                            <outputFile>${project.build.outputDirectory}/${project.artifactId}.js</outputFile>
                            <multiPlatform>true</multiPlatform>
                        </configuration>
                    </execution>
                    <execution>
                        <id>test-compile</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>test-js</goal>
                        </goals>
                        <configuration>
                            <sourceMap>true</sourceMap>
                            <moduleKind>umd</moduleKind>
                            <outputFile>${project.basedir}/target/test-js/${project.artifactId}-tests.js</outputFile>
                            <multiPlatform>true</multiPlatform>
                        </configuration>
                    </execution>
                </executions>

            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.6</version>
                <configuration>
                    <forceCreation>true</forceCreation>
                    <classesDirectory>${project.build.outputDirectory}</classesDirectory>
                    <includes>
                        <include>**/*.js</include>
                        <include>**/*.js.map</include>
                        <include>**/*.kjsm</include>
                        <include>**/*.kotlin_classes</include>
                        <include>**/*.kotlin_string_table</include>
                        <include>**/*.kotlin_file_table</include>
                    </includes>
                    <archive>
                        <forced/>
                        <manifestEntries>
                            <Built-By>${user.name}</Built-By>
                            <Implementation-Vendor>JetBrains s.r.o.</Implementation-Vendor>
                            <Implementation-Version>${project.version}</Implementation-Version>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <id>unpack-dependencies</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <includeScope>test</includeScope>
                            <excludeArtifactIds>share</excludeArtifactIds>
                            <includeTypes>jar</includeTypes>
                            <outputDirectory>${project.build.directory}/test-js</outputDirectory>
                            <includes>*.js</includes>
                            <excludes>*.meta.js</excludes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>com.github.eirslett</groupId>
                <artifactId>frontend-maven-plugin</artifactId>
                <version>1.3</version>
                <executions>
                    <execution>
                        <id>javascript tests</id>
                        <goals>
                            <goal>karma</goal>
                        </goals>
                        <configuration>
                            <karmaConfPath>${project.basedir}/src/test/karma/karma.conf.js</karmaConfPath>
                            <skip>${maven.test.skip}</skip>
                            <skipTests>${maven.test.skip}</skipTests>
                        </configuration>
                    </execution>
                </executions>
                <configuration>
                    <nodeVersion>v0.12.17</nodeVersion>
                    <npmVersion>3.10.9</npmVersion>
                    <workingDirectory>${project.basedir}/src/test/karma</workingDirectory>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <configuration>
                            <tasks>
                                <copy file="${project.build.outputDirectory}/${project.artifactId}.js"
                                      tofile="${main.file.path}"/>
                                <copy file="${project.build.outputDirectory}/${project.artifactId}.meta.js"
                                      tofile="${main.meta.file.path}"/>
                                <copy file="${project.build.outputDirectory}/package.json" todir="${project.basedir}/dist"/>
                                <copy file="${project.build.outputDirectory}/README.md" todir="${project.basedir}/dist"/>
                                <copy file="${project.build.outputDirectory}/index.d.ts" todir="${project.basedir}/dist"/>
                            </tasks>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-clean-plugin</artifactId>
                <version>3.0.0</version>
                <configuration>
                    <filesets>
                        <fileset>
                            <directory>dist</directory>
                            <includes>
                                <include>**/*.*</include>
                            </includes>
                            <followSymlinks>false</followSymlinks>
                        </fileset>
                    </filesets>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>