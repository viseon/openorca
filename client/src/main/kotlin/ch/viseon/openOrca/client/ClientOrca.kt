/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.client

import ch.viseon.openOrca.share.*
import kodando.rxjs.Rx

/**
 * The observer* methods allow a declarative approach to listen to changes.
 *
 */
interface ClientOrca {

  @JsName("observeModelStore")
  fun observeModelStore(): Rx.IObservable<ModelStoreChangeEvent>

  @JsName("observeModelById")
  fun observeModel(modelId: ModelId): Rx.IObservable<PropertyChangeEvent>

  @JsName("observeModelByType")
  fun observeModel(modelType: ModelType): Rx.IObservable<PropertyChangeEvent>

  @JsName("observeProperty")
  fun observeProperty(modelId: ModelId, propertyName: PropertyName): Rx.IObservable<ValueChangeEvent>

  @JsName("getModelById")
  fun model(modelId: ModelId): PresentationModel

  @JsName("getModelByType")
  fun model(modelType: ModelType): Array<PresentationModel>

  @JsName("registerNamedCommand")
  fun registerNamedCommand(actionName: String): Rx.IObservable<ActionEvent>

  //Process and send a series of commands.
  @JsName("executeCommands")
  fun executeCommands(commands: Array<CommandData>)

  @JsName("containsModelById")
  fun contains(modelId: ModelId): Boolean

  @JsName("containsModelByType")
  fun contains(modelId: ModelType): Boolean

  @JsName("getAllModels")
  fun getAllModels(): Array<PresentationModel>

  @JsName("observeRequest")
  fun observeRequest(): Rx.IObservable<RequestEvent>

}

enum class RequestEvent {
  START,
  END,
  ;

  fun isStart() = this == START
  fun isEnd() = this == END
}

