/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.client

import ch.viseon.openOrca.share.CommandApplier
import ch.viseon.openOrca.share.CommandData
import ch.viseon.openOrca.share.ModelStore
import ch.viseon.openOrca.share.Source
import ch.viseon.openOrca.share.command.CommandApplication
import kodando.rxjs.Rx
import kodando.rxjs.flatMap

interface CommandListExecutor {

  fun execute(modelStore: ModelStore, commandData: Rx.IObservable<CommandData>): Rx.IObservable<CommandApplication>

  fun execute(modelStore: ModelStore, commandData: CommandData): Rx.IObservable<CommandApplication> {
    return execute(modelStore, just(commandData))
  }

}

class ClientCommandListExecutor : CommandListExecutor {

  override fun execute(modelStore: ModelStore, commandData: Rx.IObservable<CommandData>): Rx.IObservable<CommandApplication> {
    return commandData.flatMap { applyCommand(modelStore, it) }
  }

  private fun applyCommand(modelStore: ModelStore, it: CommandData): Rx.IObservable<CommandApplication> {
    try {
      return Rx.Observable.of(CommandApplier.apply(Source.REQUEST, modelStore, it))
    } catch (e: Exception) {
      throw RuntimeException("Error during application of command $it.\n Original error: $e")
    }
  }
}

