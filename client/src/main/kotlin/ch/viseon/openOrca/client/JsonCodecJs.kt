/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca.client

import ch.viseon.openOrca.share.JsonObj
import kotlin.js.Json

class JsonFactoryJs: ch.viseon.openOrca.share.JsonFactory {

  override fun newJson(block: ch.viseon.openOrca.share.JsonObj.() -> Unit): ch.viseon.openOrca.share.JsonObj {
    return ch.viseon.openOrca.client.JsonObjJs(block)
  }

  override fun parse(content: String): ch.viseon.openOrca.share.JsonArray<JsonObj> {
    return ch.viseon.openOrca.share.JsonArray(JSON.parse<Array<Json>>(content).map(::JsonObjJs))
  }

}

class JsonObjJs(val json: kotlin.js.Json = kotlin.js.json()) : ch.viseon.openOrca.share.JsonObj {



  override fun integer(key: String): Int {
    return json[key].toString().toInt()
  }


  override fun long(key: String): Long {
    return json[key].toString().toLong()
  }

  override fun float(key: String): Float {
    return json[key].toString().toFloat()
  }

  override fun double(key: String): Double {
    return json[key].toString().toDouble()
  }

  constructor(block: ch.viseon.openOrca.share.JsonObj.() -> Unit) : this() {
    block()
  }

  override fun boolean(key: String): Boolean {
    return json[key] == "true"
  }

  @Suppress("UNCHECKED_CAST")
  override fun <T> pArray(key: String): ch.viseon.openOrca.share.JsonArray<T> {
    val array = (json[key] as Array<T>)
    return ch.viseon.openOrca.share.JsonArray(array.asIterable())
  }

  @Suppress("UNCHECKED_CAST")
  override fun oArray(key: String): ch.viseon.openOrca.share.JsonArray<JsonObj> {
    val values: MutableList<ch.viseon.openOrca.share.JsonObj> = (json[key] as Array<kotlin.js.Json>).map { ch.viseon.openOrca.client.JsonObjJs(it) }.toMutableList()
    return ch.viseon.openOrca.share.JsonArray(values)
  }

  override fun string(key: String): String {
    return json[key] as String
  }

  override fun optionalString(key: String): String? {
    return json[key] as String?
  }

  override fun optionalBoolean(key: String): Boolean? {
    return json[key] as Boolean?
  }

  override fun getObject(key: String): ch.viseon.openOrca.share.JsonObj {
    @Suppress("UNCHECKED_CAST_TO_NATIVE_INTERFACE")
    return ch.viseon.openOrca.client.JsonObjJs(json[key] as Json)
  }

  override fun set(key: String, value: String) {
    json[key] = value
  }

  override fun set(key: String, value: Boolean) {
    json[key] = value
  }

  override fun set(key: String, value: Array<String>) {
    json[key] = value
  }

  override operator fun set(key: String, value: ch.viseon.openOrca.share.JsonObj) {
    json[key] = (value as ch.viseon.openOrca.client.JsonObjJs).json
  }

  override fun set(key: String, value: ch.viseon.openOrca.share.JsonArray<JsonObj>) {
    json[key] = value.map { (it as ch.viseon.openOrca.client.JsonObjJs).json }
  }

  override fun set(key: String, value: Iterable<String>) {
    json[key] = value
  }

  override fun set(key: String, block: ch.viseon.openOrca.share.JsonObj.() -> Unit) {
    val subJsonObj = ch.viseon.openOrca.client.JsonObjJs()
    subJsonObj.block()
    json[key] = subJsonObj.json
  }

  override fun toJsonString(): String {
    return JSON.stringify(json)
  }


}

