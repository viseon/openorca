/* * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as Rx from "rxjs";

export as namespace openOrca

export namespace ch {
    export namespace viseon {
        export namespace openOrca {
            export namespace client {

                import JsonCodec = ch.viseon.openOrca.share.JsonCodec;
                import JsonFactory = ch.viseon.openOrca.share.JsonFactory;
                import ModelId = ch.viseon.openOrca.share.ModelId;
                import ModelType = ch.viseon.openOrca.share.ModelType;
                import ModelStoreChangeEvent = ch.viseon.openOrca.share.ModelStoreChangeEvent;
                import PropertyChangeEvent = ch.viseon.openOrca.share.PropertyChangeEvent;
                import ValueChangeEvent = ch.viseon.openOrca.share.ValueChangeEvent;
                import ActionEvent = ch.viseon.openOrca.share.ActionEvent;
                import PresentationModel = ch.viseon.openOrca.share.PresentationModel;
                import CommandData = ch.viseon.openOrca.share.CommandData;
                import PropertyName = ch.viseon.openOrca.share.PropertyName;

                export class ClientCommandListExecutor {
                    constructor();
                }

                export interface ClientOrca {

                    observeModelStore(): Rx.Observable<ModelStoreChangeEvent>;

                    observeModelById(modelId: ModelId): Rx.Observable<PropertyChangeEvent>;

                    observeModelByType(modelType: ModelType): Rx.Observable<PropertyChangeEvent>;

                    observeProperty(modelId: ModelId, propertyName: PropertyName): Rx.Observable<ValueChangeEvent>;

                    getModelById(modelId: ModelId): PresentationModel;

                    getModelByType(modelType: ModelType): Array<PresentationModel>;

                    registerNamedCommand(): Rx.Observable<ActionEvent>;

                    //Process and send a series of commands.
                    executeCommands(commands: Array<CommandData>);

                    containsModelById(modelId: ModelId): boolean

                    containsModelByType(modelType: ModelType): boolean

                    getAllModels(): Array<PresentationModel>

                    observeRequest(): Rx.Observable<RequestEvent>
                }

                //noinspection JSUnusedGlobalSymbols
                export class ClientOrcaImpl implements ClientOrca {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(commandListExecutor: ClientCommandListExecutor, transmitter: XMLHttpRequestTransmitter)

                    observeModelStore(): Rx.Observable<ModelStoreChangeEvent>;

                    observeModelById(modelId: ch.viseon.openOrca.share.ModelId): Rx.Observable<PropertyChangeEvent>;

                    observeModelByType(modelType: ch.viseon.openOrca.share.ModelType): Rx.Observable<PropertyChangeEvent>;

                    observeProperty(modelId: ModelId, propertyName: PropertyName): Rx.Observable<ValueChangeEvent>;

                    getModelById(modelId: ch.viseon.openOrca.share.ModelId): PresentationModel;

                    getModelByType(modelType: ch.viseon.openOrca.share.ModelType): Array<PresentationModel>;

                    registerNamedCommand(): Rx.Observable<ActionEvent>;

                    executeCommands(commands: Array<any>);

                    containsModelById(modelid: ModelId): boolean;

                    containsModelByType(modelType: ModelType): boolean;

                    getAllModels(): Array<ch.viseon.openOrca.share.PresentationModel>;

                    observeRequest(): Rx.Observable<RequestEvent>
                }

                export class RequestEvent {
                    //noinspection JSUnusedGlobalSymbols
                    static readonly START: RequestEvent;
                    //noinspection JSUnusedGlobalSymbols
                    static readonly END: RequestEvent;

                    //noinspection JSUnusedGlobalSymbols
                    isEnd(): boolean

                    //noinspection JSUnusedGlobalSymbols
                    isStart(): boolean
                }

                export class XMLHttpRequestTransmitter {
                    constructor(url: string, codec: JsonCodec)
                }

                //noinspection JSUnusedGlobalSymbols
                export class JsonFactoryJs extends JsonFactory {
                    //noinspection JSUnusedGlobalSymbols
                    constructor();
                }

            }

            export namespace share {

                export class Tag {
                    constructor(name: string)

                    //noinspection JSUnusedGlobalSymbols
                    name: string
                }

                export class Source {
                    //noinspection JSUnusedGlobalSymbols
                    static readonly REQUEST: Source;
                    //noinspection JSUnusedGlobalSymbols
                    static readonly RESPONSE: Source;

                    //noinspection JSUnusedGlobalSymbols
                    isRequest(): boolean;

                    //noinspection JSUnusedGlobalSymbols
                    isResponse(): boolean;
                }

                //noinspection JSUnusedGlobalSymbols
                export class ApplyPolicy {
                    //noinspection JSUnusedGlobalSymbols
                    static readonly DEFAULT: ApplyPolicy;
                    //noinspection JSUnusedGlobalSymbols
                    static readonly FORCE_SEND: ApplyPolicy;
                    //noinspection JSUnusedGlobalSymbols
                    static readonly FORCE_APPLY: ApplyPolicy;

                    //noinspection JSUnusedGlobalSymbols
                    isForceSend(): boolean;

                    //noinspection JSUnusedGlobalSymbols
                    isForceApply(): boolean;

                    //noinspection JSUnusedGlobalSymbols
                    isDefault(): boolean;

                    //noinspection JSUnusedGlobalSymbols
                    name: string
                }

                //noinspection JSUnusedGlobalSymbols
                export class PresentationModelBuilder {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(modelId: ModelId, modelType: ModelType, clientPm: boolean = false, block)

                    //noinspection JSUnusedGlobalSymbols
                    property_o74rtj$(name: PropertyName, block: (propertyBuilder: PropertyBuilder) => void)

                    //noinspection JSUnusedGlobalSymbols
                    build_9e13n0$(source: Source, forceSend: boolean = false)
                }

                export class PropertyBuilder {
                    set_jcm6gy$(tag: Tag, value: any)
                }

                export interface JsonFactory {

                }

                export class JsonCodec {
                    constructor(jsonFactory: JsonFactory)
                }

                export interface PresentationModel {
                    id: ModelId;
                    type: ModelType;

                    getPropertiesArray(): Array<Property>

                    getProperties(): any //Kotlin.Sequence<Property>
                    get_lqljjj$(propertyName: PropertyName): Property

                    hasProperty_lqljjj$(name: PropertyName): boolean
                }

                //noinspection JSUnusedGlobalSymbols
                export class DefaultPresentationModel implements PresentationModel {
                    //noinspection JSUnusedGlobalSymbols
                    id: ModelId;
                    //noinspection JSUnusedGlobalSymbols
                    type: ModelType;
                    //noinspection JSUnusedGlobalSymbols
                    clientOnly: boolean;

                    getPropertiesArray(): Array<Property>

                    getProperties(): any //Kotlin.Sequence<Property>
                    get_lqljjj$(propertyName: PropertyName): Property

                    hasProperty_lqljjj$(name: PropertyName): boolean
                }

                export class ModelId {
                    constructor(stringId: string)

                    //noinspection JSUnusedGlobalSymbols
                    stringId: string;
                }

                export class ModelType {
                    constructor(stringId: string)

                    //noinspection JSUnusedGlobalSymbols
                    stringId: string;
                }

                export class PropertyName {
                    constructor(name: string)

                    //noinspection JSUnusedGlobalSymbols
                    name: string;
                }

                export class Property {
                    constructor()

                    //noinspection JSUnusedGlobalSymbols
                    name: PropertyName;
                    //noinspection JSUnusedGlobalSymbols
                    clientOnly: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    clientTags: any; //Kotlin Set<Tag>

                    //noinspection JSUnusedGlobalSymbols
                    isClientTag(tag: Tag): boolean

                    //noinspection JSUnusedGlobalSymbols
                    getValuesArray(): Array<any>

                    //noinspection JSUnusedGlobalSymbols
                    getValues(): any //kotlin.sequence<Pair<Tag, any>>
                    //noinspection JSUnusedGlobalSymbols
                    get_ol557t$(valueType: Tag): any

                    //noinspection JSUnusedGlobalSymbols
                    getValue_vd0u4k$(valueType: Tag): any //Tries to cast the value - DO NOT USE
                    //noinspection JSUnusedGlobalSymbols
                    hasValue_ol557t$(tag: Tag): boolean
                }

                export interface CommandData {
                    source: Source
                    policy: ApplyPolicy
                }

                //noinspection JSUnusedGlobalSymbols
                export class ChangeValueCommandData implements CommandData {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(source: Source, modelId: ModelId, propertyName: PropertyName, tag: Tag, value: any, forceSend: boolean = false);

                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    modelId: ModelId;
                    //noinspection JSUnusedGlobalSymbols
                    propertyName: PropertyName;
                    //noinspection JSUnusedGlobalSymbols
                    value: any;
                    //noinspection JSUnusedGlobalSymbols
                    tag: Tag;
                    //noinspection JSUnusedGlobalSymbols
                    policy: ApplyPolicy
                }

                //noinspection JSUnusedGlobalSymbols
                export class CreateModelCommandData implements CommandData {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(source: Source, modelId: ModelId, modelType: ModelType, properties: Array<Property>, clientOnly: boolean = false, applyPolicy: ApplyPolicy = ApplyPolicy.DEFAULT)

                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    modelId: ModelId;
                    //noinspection JSUnusedGlobalSymbols
                    modelType: ModelType;
                    //noinspection JSUnusedGlobalSymbols
                    properties: Array<Property>;
                    //noinspection JSUnusedGlobalSymbols
                    policy: ApplyPolicy
                }

                export class CompositeCommand implements CommandData {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(source: Source, commands: any /*kotlin.list*/)
                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    policy: ApplyPolicy;
                    //noinspection JSUnusedGlobalSymbols
                    commands: any /*kotlin.list*/
                }

                //noinspection JSUnusedGlobalSymbols
                export class RemoveModelCommandData implements CommandData {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(source: Source, modelId: ModelId, modelType: ModelType, forceSend: boolean = false)

                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    modelId: ModelId;
                    //noinspection JSUnusedGlobalSymbols
                    policy: ApplyPolicy
                }

                //noinspection JSUnusedGlobalSymbols
                export class ActionCommandData implements CommandData {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(source: Source, actionName: string, modelIds: any /*kotlin.list<ModelId>*/, forceSend: Boolean = false);

                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    actionName: String;
                    //noinspection JSUnusedGlobalSymbols
                    modelIds: any; //kotlin.list<ModelId>
                    //noinspection JSUnusedGlobalSymbols
                    policy: ApplyPolicy
                }

                /**
                 * Removes all models of a certain type.
                 */
                    //noinspection JSUnusedGlobalSymbols
                export class RemoveModelByTypeCommandData implements CommandData {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(source: Source, modelType: ModelType, forceSend: boolean = false)

                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    modelType: ModelType;
                    //noinspection JSUnusedGlobalSymbols
                    policy: ApplyPolicy
                }

                /**
                 * Copy one model onto another.
                 */
                    //noinspection JSUnusedGlobalSymbols
                export class SyncModelCommandData implements CommandData {
                    //noinspection JSUnusedGlobalSymbols
                    constructor(source: Source, sourceModel: ModelId, destinationModel: ModelId, forceSend: boolean = false)

                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    sourceModel: ModelId;
                    //noinspection JSUnusedGlobalSymbols
                    destinationModel: ModelId;
                    //noinspection JSUnusedGlobalSymbols
                    policy: ApplyPolicy
                }

                export interface Event {
                    source: Source
                    isCompositeUpdate: boolean
                    isLastEventOfCompositeUpdate: boolean
                }

                export class ModelStoreChangeEvent implements Event {
                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    isCompositeUpdate: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    isLastEventOfCompositeUpdate: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    modelId: ModelId;
                    //noinspection JSUnusedGlobalSymbols
                    modelType: ModelType;
                    //noinspection JSUnusedGlobalSymbols
                    eventType: ModelStoreChangeEventType;
                }

                export class ModelStoreChangeEventType {
                    //noinspection JSUnusedGlobalSymbols
                    isAdd(): boolean

                    //noinspection JSUnusedGlobalSymbols
                    isRemove(): boolean
                }

                export class PropertyChangeEvent implements Event {
                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    isCompositeUpdate: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    isLastEventOfCompositeUpdate: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    modelId: ModelId;
                    //noinspection JSUnusedGlobalSymbols
                    modelType: ModelType;
                    //noinspection JSUnusedGlobalSymbols
                    valueChangeEvent: ValueChangeEvent
                }

                export class ValueChangeEvent implements Event {
                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    isCompositeUpdate: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    isLastEventOfCompositeUpdate: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    property: PropertyName;
                    //noinspection JSUnusedGlobalSymbols
                    tag: Tag;
                    //noinspection JSUnusedGlobalSymbols
                    oldValue: any;
                    //noinspection JSUnusedGlobalSymbols
                    newValue: any;

                    //noinspection JSUnusedGlobalSymbols
                    isValueChange(): boolean;

                    //noinspection JSUnusedGlobalSymbols
                    isLabelChange(): boolean;

                    //noinspection JSUnusedGlobalSymbols
                    isToolTipChange(): boolean;
                }

                export class ActionEvent implements Event {
                    //noinspection JSUnusedGlobalSymbols
                    source: Source;
                    //noinspection JSUnusedGlobalSymbols
                    isCompositeUpdate: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    isLastEventOfCompositeUpdate: boolean;
                    //noinspection JSUnusedGlobalSymbols
                    actionName: String;
                    //noinspection JSUnusedGlobalSymbols
                    pmIds: Array<ModelId>
                }
            } //End share
        }
    }
}



