/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class CommandListExecutorTest : AbstractCommandApplierTest() {

  @Test
  override fun syncWithModel() {
    super.syncWithModel()
  }

  @Test
  override fun changeValueCommandIsApplied() {
    super.changeValueCommandIsApplied()
  }

  @Test
  override fun changeValueTwiceDoesNotGenerateASecondEvent() {
    super.changeValueTwiceDoesNotGenerateASecondEvent()
  }

  @Test
  override fun removeModelApplyTwiceIsNotApplied() {
    super.removeModelApplyTwiceIsNotApplied()
  }

  @Test
  override fun removeModelByType() {
    super.removeModelByType()
  }

  @Test
  override fun removeModelByTypeApplyTwiceIsNotApplied() {
    super.removeModelByTypeApplyTwiceIsNotApplied()
  }

  @Test
  override fun createModelApplyTwiceIsNotApplied() {
    super.createModelApplyTwiceIsNotApplied()
  }

  @Test
  override fun applyCommandWithDifferentSourceIfItIsForceSend() {
    super.applyCommandWithDifferentSourceIfItIsForceSend()
  }

  @Test
  override fun forceApplyChangeValueCommand() {
    super.forceApplyChangeValueCommand()
  }

  @Test
  override fun syncValueCommand() {
    super.syncValueCommand()
  }

  @Test
  override fun decorateExceptionIfCommandApplicationFails() {
    super.decorateExceptionIfCommandApplicationFails()
  }

  @Test
  override fun compositeCommand() {
    super.compositeCommand()
  }

  @Test
  fun changeValueForClientModel() {
    //Setup
    val modelStore = SharedTestData.createModelStore(Source.REQUEST, SharedTestData.createClientPm(Source.REQUEST))

    //Execution
    val newValue = "NewValue"
    CommandApplier.apply(Source.REQUEST,
            modelStore,
            ChangeValueCommandData(
                    Source.REQUEST,
                    SharedTestData.clientModelId,
                    SharedTestData.clientProperty,
                    Tag.LABEL,
                    newValue
            ))


    //Assertion
    modelStore[SharedTestData.clientModelId][SharedTestData.clientProperty].also { property ->
      assertTrue(property.hasValue(Tag.LABEL))
      assertEquals(newValue, property[Tag.LABEL])
    }

  }

  @Test
  fun changeValueForClientProperty() {
    //Setup
    val modelStore = SharedTestData.createModelStore(Source.REQUEST,
            SharedTestData.createPmWithClientProperty(Source.REQUEST))

    //Execution
    val newValue = "NewValue"
    CommandApplier.apply(Source.REQUEST,
            modelStore,
            ChangeValueCommandData(
                    Source.REQUEST,
                    SharedTestData.clientModelId,
                    SharedTestData.clientProperty,
                    Tag.LABEL,
                    newValue
            ))

    //assertion
    assertEquals(newValue, modelStore[SharedTestData.clientModelId][SharedTestData.clientProperty][Tag.LABEL])
  }

  @Test
  fun changeValueForClientTag() {
    //Setup
    val modelStore = SharedTestData.createModelStore(Source.REQUEST, SharedTestData.createPmWithClientTag(Source.REQUEST))

    //Execution
    val newValue = "NewValue"
    val commandApplication = CommandApplier.apply(Source.REQUEST,
            modelStore,
            ChangeValueCommandData(
                    Source.REQUEST,
                    SharedTestData.clientModelId,
                    SharedTestData.clientProperty,
                    Tag.LABEL,
                    newValue
            ))


    //Assertion
    assertTrue(commandApplication.applied, "Command should be applied")
    assertFalse(commandApplication.sendCommand(), "Command should not be send to the server")
    assertTrue(commandApplication.events.count() > 0, "Event for client command missing")

    modelStore[SharedTestData.clientModelId][SharedTestData.clientProperty].also { property ->
      assertTrue(property.hasValue(Tag.LABEL))
      assertEquals(newValue, property[Tag.LABEL])
    }

  }

  @Test
  fun createClientPm() {
    //Setup
    val modelStore = DefaultPresentationModelStore()

    //Execution
    val commandApplication = CommandApplier.apply(Source.REQUEST,
            modelStore,
            SharedTestData.createClientPm(Source.REQUEST))


    //Assertion
    assertTrue(commandApplication.applied, "client command should be executed on the client side")
    assertFalse(commandApplication.sendCommand(), "client command must not be sent to the server")
    assertTrue(commandApplication.events.count() == 1, "Event from application expected")

    assertTrue(modelStore.contains(SharedTestData.clientModelId), "client pm must be in store")
    assertTrue(modelStore[SharedTestData.clientModelId].clientOnly, "Pm should be marked as a client onl pm")
    assertEquals(1, modelStore[SharedTestData.clientModelId].propertyCount())
  }
}