/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.share.AbstractModelStoreTest
import kotlin.test.Test

class ModelStoreTest: AbstractModelStoreTest() {

  @Test
  override fun containsByType() {
    super.containsByType()
  }

  @Test
  override fun getAllModels() {
    super.getAllModels()
  }

  @Test
  override fun getAllModelsHonorsInsertionOrder() {
    super.getAllModelsHonorsInsertionOrder()
  }

  @Test
  override fun removeModel() {
    super.removeModel()
  }

  @Test
  override fun containsModel() {
    super.containsModel()
  }

  @Test
  override fun containsModelByType() {
    super.containsModelByType()
  }

  @Test
  override fun getModelById() {
    super.getModelById()
  }

  @Test
  override fun getModelByTypeId() {
    super.getModelByTypeId()
  }

  @Test
  override fun removeModelByType() {
    super.removeModelByType()
  }
}