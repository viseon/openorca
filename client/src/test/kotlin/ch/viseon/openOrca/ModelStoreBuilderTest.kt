/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.share.PresentationModelBuilder
import ch.viseon.openOrca.share.SharedTestData
import ch.viseon.openOrca.share.Source
import kotlin.test.Test
import kotlin.test.assertEquals

class ModelStoreBuilderTest {

  @Test
  fun buildModel() {
    val builder = PresentationModelBuilder(
        SharedTestData.modelId,
        SharedTestData.modelType) {
      property(SharedTestData.prop1) {}
    }

    val commandData = builder.build(Source.REQUEST)
    assertEquals(SharedTestData.modelId, commandData.modelId)
    assertEquals(SharedTestData.modelType, commandData.modelType)
    val property = commandData.properties.first()
    assertEquals(SharedTestData.prop1, property.name)
  }

}
