/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.client.ClientCommandListExecutor
import ch.viseon.openOrca.client.ClientOrcaImpl
import ch.viseon.openOrca.client.RequestEvent
import ch.viseon.openOrca.client.Transmitter
import ch.viseon.openOrca.share.ApplyPolicy
import ch.viseon.openOrca.share.CommandData
import ch.viseon.openOrca.share.SharedTestData
import ch.viseon.openOrca.share.Source
import kodando.rxjs.Rx
import kodando.rxjs.filter
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ClientOrcaImplTest {

  private fun createOrcaImpl() = ClientOrcaImpl(
          ClientCommandListExecutor(),
          NoTransmitter(Rx.Observable.Companion.empty())
  )

  @Test
  fun modelAddedEvent() {
    var executed = false
    val impl = createOrcaImpl()
    impl
            .observeModelStore()
            .filter { it.modelId == SharedTestData.modelId }
            .subscribe {
              executed = true
            }

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty())
    ))
    assertTrue(executed, "Model event not received")
  }

  @Test
  fun changeProperty() {
    var counter = 0
    val impl = createOrcaImpl()
    impl
            .observeProperty(SharedTestData.modelId, SharedTestData.prop1)
            .subscribe {
              ++counter
            }

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithTwoProperties()),
            SharedTestData.changeValueCommand(Source.REQUEST),
            SharedTestData.changeValueCommand2(Source.REQUEST)
    ))
    assertEquals(1, counter, "Amount of property change event not correct")
  }

  @Test
  fun modelChangeEvent() {
    var executed = false
    val impl = createOrcaImpl()
    impl
            .observeModel(SharedTestData.modelId)
            .subscribe {
              executed = true
            }

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty()),
            SharedTestData.changeValueCommand()
    ))
    assertTrue(executed, "Model event not received")
  }

  @Test
  fun syncCommandNotifiesObserveModel() {
    var executed = false
    val impl = createOrcaImpl()
    impl
            .observeModel(SharedTestData.modelId2)
            .subscribe {
              executed = true
            }

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty()),
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty(SharedTestData.modelType, SharedTestData.modelId2)),
            SharedTestData.syncModelCommand()
    ))
    assertTrue(executed, "Model event not received")
  }

  @Test
  fun dontGenerateEventsForNotAppliedCommands() {
    var counter = 0
    val impl = createOrcaImpl()
    impl
            .observeModelStore()
            .subscribe {
              ++counter
            }

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty()),
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty())
    ))

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty())
    ))
    assertEquals(1, counter, "Incorrect number of events received")
  }


  @Test
  fun dontSendCommandsForNotAppliedCommands() {
    var counter = 0
    val impl = ClientOrcaImpl(ClientCommandListExecutor(), object : Transmitter {
      override fun sendCommands(commands: List<CommandData>): Rx.IObservable<CommandData> {
        if (commands.count() > 0) {
          ++counter
        }
        return Rx.Observable.Companion.empty()
      }
    })

    impl
            .observeModelStore()
            .subscribe {
              //nothing to do
            }

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty())
    ))

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty())
    ))

    assertEquals(1, counter, "Not applied commands should not be send to the server")
  }

  @Test
  fun applyForceSendCommandsFromController() {
    val impl = ClientOrcaImpl(ClientCommandListExecutor(), object : Transmitter {
      override fun sendCommands(commands: List<CommandData>): Rx.IObservable<CommandData> {
        return Rx.Observable.Companion.of(
                SharedTestData.changeValueCommand(source = Source.RESPONSE, policy = ApplyPolicy.FORCE_SEND), //Should be send as is
                SharedTestData.changeValueCommand(source = Source.RESPONSE, policy = ApplyPolicy.FORCE_SEND)
        )
      }
    })

    var counter = 0
    impl
            .observeModel(SharedTestData.modelId)
            .subscribe {
              ++counter
            }

    impl.executeCommands(arrayOf(
            SharedTestData.createModelCommand(SharedTestData.createPmWithOneProperty())
    ))

    assertEquals(2, counter, "Force command was not applied. ")
  }

  @Test
  fun testObserverRequestTracker() {
    val impl = createOrcaImpl()

    val expectedEvents = listOf(RequestEvent.START, RequestEvent.END)
    val actualEvents = mutableListOf<RequestEvent>()
    impl.observeRequest()
            .subscribe { actualEvents.add(it) }

    impl.executeCommands(arrayOf(SharedTestData.createModelCommand()))

    assertEquals(expectedEvents, actualEvents)
  }

  @Test
  fun clientCommandNotSend() {
    var transmitterCalled = false
    val impl = ClientOrcaImpl(ClientCommandListExecutor(), object: Transmitter {
      override fun sendCommands(commands: List<CommandData>): Rx.IObservable<CommandData> {
        transmitterCalled = true
        //Command should not be send to controller
        assertEquals(0, commands.count())
        return Rx.Observable.Companion.empty()
      }
    })

    impl.executeCommands(arrayOf(SharedTestData.createClientPm(Source.REQUEST)))

    assertTrue(transmitterCalled)
  }

}

private class NoTransmitter(val commandsToReturn: Rx.IObservable<CommandData>) : Transmitter {
  override fun sendCommands(commands: List<CommandData>): Rx.IObservable<CommandData> {
    return commandsToReturn
  }

}