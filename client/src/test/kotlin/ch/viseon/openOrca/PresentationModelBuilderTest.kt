/*
 * Copyright 2017 viseon gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.viseon.openOrca

import ch.viseon.openOrca.share.*
import ch.viseon.openOrca.share.impl.DefaultPresentationModelStore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class PresentationModelBuilderTest: AbstractModelBuilderTest() {

  @Test
  override fun buildModelWithOneProperty() {
    super.buildModelWithOneProperty()
  }

  @Test
  override fun buildModelWithSeveralPropertiesPreserveOrder() {
    super.buildModelWithSeveralPropertiesPreserveOrder()
  }

  @Test
  fun createClientOnlyModel() {
    val modelStore = DefaultPresentationModelStore()
    CommandApplier.apply(Source.REQUEST, modelStore, SharedTestData.createClientPm(Source.RESPONSE))

    //Assert
    modelStore[SharedTestData.clientModelId].also {
      assertEquals(1, it.propertyCount())
      assertTrue(it.hasProperty(SharedTestData.clientProperty))
      assertTrue(it[SharedTestData.clientProperty].clientOnly)
      assertTrue(it.clientOnly)
    }
  }

  @Test
  fun createPmWithClientProperty() {
    val modelStore = DefaultPresentationModelStore()
    CommandApplier.apply(Source.REQUEST, modelStore, SharedTestData.createPmWithClientProperty(Source.RESPONSE))

    //Assert
    modelStore[SharedTestData.clientModelId].also {
      assertEquals(1, it.propertyCount())
      assertTrue(it.hasProperty(SharedTestData.clientProperty))
      assertTrue(it[SharedTestData.clientProperty].clientOnly)
      assertFalse(it.clientOnly)
    }
  }

  @Test
  fun createPWithClientTag() {
    val modelStore = DefaultPresentationModelStore()
    CommandApplier.apply(Source.REQUEST, modelStore, SharedTestData.createPmWithClientTag(Source.RESPONSE))

    //Assert
    modelStore[SharedTestData.clientModelId].also {
      assertEquals(1, it.propertyCount())
      assertTrue(it[SharedTestData.clientProperty].clientTags.contains(Tag.LABEL))
      assertTrue(it.hasProperty(SharedTestData.clientProperty))
      assertFalse(it.clientOnly)
    }
  }
}

